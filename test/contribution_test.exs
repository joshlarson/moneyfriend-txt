defmodule ContributionTest do
  use ExUnit.Case
  doctest Moneyfriend

  describe "with some contributions from a paycheck to other categories" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Contribute $200 From Paycheck To Health",
            "2020/01/01 Contribute $300 From Paycheck To TaxWithholding"
          ]
          |> Moneyfriend.process()
      }
    end

    test "contributions count against the budget that they're contributed 'To'", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Health") ==
               [
                 "-$200.00",
                 "          -        $200.00    2020/01/01    Contrib. From   Paycheck",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("TaxWithholding") ==
               [
                 "-$300.00",
                 "          -        $300.00    2020/01/01    Contrib. From   Paycheck",
                 "$0.00"
               ]
    end

    test "contributions count towards the budget that they're contributed 'From'", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Paycheck") ==
               [
                 "$500.00",
                 "          +        $300.00    2020/01/01    Contrib. To     TaxWithholding",
                 "$200.00",
                 "          +        $200.00    2020/01/01    Contrib. To     Health",
                 "$0.00"
               ]
    end

    test "has an empty summary (no warnings)", context do
      assert context[:friend] |> Moneyfriend.summary() == []
    end
  end
end
