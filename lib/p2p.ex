defmodule P2P do
  defstruct ledger: [Money.new(0, :USD)],
            funding_bank_name: nil

  def new(starting_balance, funding_bank_name) do
    %P2P{
      funding_bank_name: funding_bank_name,
      ledger: Ledger.new(starting_balance)
    }
  end

  def balance(%P2P{ledger: [balance | _]}) do
    balance
  end

  def receive(p2p = %P2P{ledger: ledger}, transaction) do
    %P2P{p2p | ledger: ledger |> Ledger.apply_transaction(transaction)}
  end

  def pay(
        p2p = %P2P{ledger: ledger = [old_balance | _], funding_bank_name: funding_bank_name},
        transaction = %Transaction{amount: amount}
      ) do
    case Money.subtract(old_balance, amount) do
      %{amount: new_cents} when new_cents < 0 ->
        new_transaction = mark_as_overdraft(transaction, funding_bank_name)

        {:overdraft, funding_bank_name,
         %P2P{p2p | ledger: ledger |> Ledger.apply_transaction(new_transaction)}}

      _ ->
        %P2P{
          p2p
          | ledger: ledger |> Ledger.apply_transaction(transaction |> Transaction.reverse())
        }
    end
  end

  def update_funding(p2p = %P2P{}, bank_name) do
    %P2P{p2p | funding_bank_name: bank_name}
  end

  defp mark_as_overdraft(
         transaction = %Transaction{amount: amount, vendor: vendor},
         funding_bank_name
       ) do
    %Transaction{
      transaction
      | amount: Money.new(0, :USD),
        vendor: "#{vendor} / #{Money.to_string(amount)} from #{funding_bank_name} Bank"
    }
  end
end
