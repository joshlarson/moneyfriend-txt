defmodule SplitwiseTest do
  use ExUnit.Case

  describe "with some split transactions and a starting balance" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Chase $0",
            "2020/01/01 Add Splitwise Peeps $0",
            "2020/01/01 Add Splitwise Fronds $10",
            "2020/01/20 Charge Chase $30 InternetBill Comcast",
            "2020/01/20 Splitwise Pay $30 Fronds 40.5% InternetBill Comcast",
            "2020/01/20 Splitwise Owe $30 Peeps 30.5% Restaurant CozyCorner"
          ]
          |> Moneyfriend.process()
      }
    end

    test "has the right ledger for Fronds", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Fronds") ==
               [
                 "$27.85",
                 "          +         $17.85    2020/01/20    Comcast         Pay $30.00 (My share was $12.15)",
                 "$10.00"
               ]
    end

    test "has the right ledger for Peeps", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Peeps") ==
               [
                 "-$9.15",
                 "          -          $9.15    2020/01/20    CozyCorner      Owe $9.15 (Total was $30.00)",
                 "$0.00"
               ]
    end

    test "shows the correct balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Chase: $30.00",
                 "",
                 "Splitwises",
                 "  Fronds: You are owed $27.85",
                 "  Peeps: You owe $9.15"
               ]
    end

    test "budget ledgers show the original charge and the Splitwise 'reimbursement'", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("InternetBill") ==
               [
                 "-$12.15",
                 "          +         $17.85    2020/01/20    Comcast         Splitwise / Fronds / Paid $30.00, my share was $12.15",
                 "-$30.00",
                 "          -         $30.00    2020/01/20    Comcast         Chase Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$9.15",
                 "          -          $9.15    2020/01/20    CozyCorner      Splitwise / Peeps / 30.5% of $30.00",
                 "$0.00"
               ]
    end
  end

  describe "with some transactions whose split is in dollars instead of percents" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Chase $0",
            "2020/01/01 Add Splitwise Peeps $0",
            "2020/01/01 Add Splitwise Fronds $0",
            "2020/01/20 Charge Chase $30 InternetBill Comcast",
            "2020/01/20 Splitwise Pay $30 Fronds $12 InternetBill Comcast",
            "2020/01/20 Splitwise Owe Peeps $9 Restaurant CozyCorner"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows the correct balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Chase: $30.00",
                 "",
                 "Splitwises",
                 "  Fronds: You are owed $18.00",
                 "  Peeps: You owe $9.00"
               ]
    end

    test "has the right ledger for Fronds", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Fronds") ==
               [
                 "$18.00",
                 "          +         $18.00    2020/01/20    Comcast         Pay $30.00 (My share was $12.00)",
                 "$0.00"
               ]
    end

    test "has the right ledger for Peeps", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Peeps") ==
               [
                 "-$9.00",
                 "          -          $9.00    2020/01/20    CozyCorner      Owe $9.00",
                 "$0.00"
               ]
    end

    test "budget ledgers show the original charge and the Splitwise 'reimbursement'", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("InternetBill") ==
               [
                 "-$12.00",
                 "          +         $18.00    2020/01/20    Comcast         Splitwise / Fronds / Paid $30.00, my share was $12.00",
                 "-$30.00",
                 "          -         $30.00    2020/01/20    Comcast         Chase Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$9.00",
                 "          -          $9.00    2020/01/20    CozyCorner      Splitwise / Peeps",
                 "$0.00"
               ]
    end
  end

  describe "closing a Splitwise" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Chase $0",
            "2020/01/01 Add Splitwise Peeps $0",
            "2020/01/01 Add Splitwise Fronds $10",
            "2020/01/20 Splitwise Owe $30 Peeps 30% Restaurant CozyCorner",
            "2020/01/30 Splitwise Pay $18 Peeps 50% Coffee Starbucks",
            "2020/01/31 Close Splitwise Peeps"
          ]
          |> Moneyfriend.process()
      }
    end

    test "has the right ledger for Fronds", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Fronds") ==
               [
                 "$10.00"
               ]
    end

    test "has removed the ledger for Peeps", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Peeps") == nil
    end

    test "does not include Peeps in the summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Chase: $0.00",
                 "",
                 "Splitwises",
                 "  Fronds: You are owed $10.00"
               ]
    end
  end

  describe "trying to close a non-settled Splitwise" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Chase $0",
            "2020/01/01 Add Splitwise Peeps $0",
            "2020/01/01 Add Splitwise Fronds $10",
            "2020/01/20 Splitwise Owe $30 Peeps 30% Restaurant CozyCorner",
            "2020/01/30 Splitwise Pay $20 Peeps 50% Coffee Starbucks",
            "2020/01/31 Close Splitwise Peeps"
          ]
          |> Moneyfriend.process()
      }
    end

    test "issues a warning", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Chase: $0.00",
                 "",
                 "Splitwises",
                 "  Fronds: You are owed $10.00",
                 "  Peeps: You are owed $1.00",
                 "",
                 "Warnings",
                 "  Cannot close non-settled Splitwise group 'Peeps'"
               ]
    end

    test "has the right ledger for Fronds", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Fronds") ==
               [
                 "$10.00"
               ]
    end

    test "still has the right ledger for Peeps", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Peeps") ==
               [
                 "$1.00",
                 "          +         $10.00    2020/01/30    Starbucks       Pay $20.00 (My share was $10.00)",
                 "-$9.00",
                 "          -          $9.00    2020/01/20    CozyCorner      Owe $9.00 (Total was $30.00)",
                 "$0.00"
               ]
    end
  end

  describe "trying to close a non-existent Splitwise" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Chase $0",
            "2020/01/01 Add Splitwise Peeps $0",
            "2020/01/01 Add Splitwise Fronds $10",
            "2020/01/20 Splitwise Owe $30 Peeps 30% Restaurant CozyCorner",
            "2020/01/30 Splitwise Pay $18 Peeps 50% Coffee Starbucks",
            "2020/01/31 Close Splitwise Pleeps"
          ]
          |> Moneyfriend.process()
      }
    end

    test "issues a warning", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Chase: $0.00",
                 "",
                 "Splitwises",
                 "  Fronds: You are owed $10.00",
                 "  Peeps: Settled up",
                 "",
                 "Warnings",
                 "  Cannot close non-existent Splitwise group 'Pleeps'"
               ]
    end

    test "has the right ledgers for both Splitwises", context do
      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Fronds") ==
               [
                 "$10.00"
               ]

      assert context[:friend] |> Moneyfriend.splitwise_ledgers() |> Map.get("Peeps") ==
               [
                 "$0.00",
                 "          +          $9.00    2020/01/30    Starbucks       Pay $18.00 (My share was $9.00)",
                 "-$9.00",
                 "          -          $9.00    2020/01/20    CozyCorner      Owe $9.00 (Total was $30.00)",
                 "$0.00"
               ]
    end
  end
end
