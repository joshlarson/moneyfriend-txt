defmodule Main do
  def main([input_file_name]) do
    friend = build_friend(input_file_name)
    print_friend_summaries(friend)
  end

  def main([input_file_name, output_folder_name]) do
    friend = build_friend(input_file_name)
    print_friend_summaries(friend)
    write_friend_details(friend, output_folder_name)
  end

  defp build_friend(input_file_name) do
    {:ok, content} = File.read(input_file_name)

    content
    |> String.split("\n")
    |> Moneyfriend.process()
  end

  defp print_friend_summaries(friend) do
    friend |> Moneyfriend.summary() |> Enum.join("\n") |> IO.puts()
  end

  defp write_friend_details(friend, output_folder_name) do
    File.rm_rf(output_folder_name)

    friend |> Moneyfriend.cash_ledger() |> write_data("#{output_folder_name}/cash", "Ledger")

    friend |> Moneyfriend.card_ledgers() |> write_details(output_folder_name, "cards")
    friend |> Moneyfriend.bank_ledgers() |> write_details(output_folder_name, "banks")
    friend |> Moneyfriend.p2p_ledgers() |> write_details(output_folder_name, "p2ps")
    friend |> Moneyfriend.splitwise_ledgers() |> write_details(output_folder_name, "splitwise")

    friend
    |> Moneyfriend.budgets_by_month()
    |> write_details(output_folder_name, "budgets_by_month")

    friend
    |> Moneyfriend.budgets_by_year()
    |> write_details(output_folder_name, "budgets_by_year")

    friend
    |> Moneyfriend.budgets_by_category_per_month()
    |> write_details(output_folder_name, "budgets_by_category_per_month")

    friend
    |> Moneyfriend.budgets_by_category_per_year()
    |> write_details(output_folder_name, "budgets_by_category_per_year")

    friend
    |> Moneyfriend.brokerage_balance_ratios_total()
    |> write_data("#{output_folder_name}/brokerages", "balance_ratios_total")

    friend
    |> Moneyfriend.brokerage_details()
    |> Enum.each(fn {brokerage_name, details} ->
      details |> write_details("#{output_folder_name}/brokerages", brokerage_name)
    end)

    friend
    |> Moneyfriend.budget_ledgers_by_month()
    |> Enum.each(fn {budget_category, ledgers} ->
      ledgers |> write_details("#{output_folder_name}/budget_ledgers_by_month", budget_category)
    end)

    friend
    |> Moneyfriend.budget_ledgers_by_year()
    |> Enum.each(fn {budget_category, ledgers} ->
      ledgers |> write_details("#{output_folder_name}/budget_ledgers_by_year", budget_category)
    end)
  end

  defp write_details(details, output_folder_name, detail_name) do
    dir_name = "#{output_folder_name}/#{detail_name}"

    details
    |> Enum.each(fn {card_name, detail} ->
      detail |> write_data(dir_name, card_name)
    end)
  end

  defp write_data(data, dir_name, file_name) do
    File.mkdir_p(dir_name)

    File.write(
      "#{dir_name}/#{file_name |> String.replace("/", "_")}.txt",
      data |> Enum.join("\n")
    )
  end
end
