# Moneyfriend

A friend for your money

## Running

To build:

```
mix escript.build
```

Once you've built your input file (see below), then run with:

```
./moneyfriend <input_file>
```

If you want to include detailed ledger output files as well, then include the name of the folder where you want your output files to go; then, output files will get written into a series of nested folders within that output folder:

```
./moneyfriend <input_file> <output_folder_path>
```

Run the tests: 

```
mix test
```


Run the tests using `entr`:

```
while true; do ls lib/*.ex test/*.exs | entr -cd bash -c "mix test"; sleep 1; done
```

# Features

_"(NYI)" means "not yet implemented", so things with that annotation are aspirational._

## Input

This section describes the things you can put in your input file. Currently, any lines of input that don't match a format that is supported (and already implemented, which means "NYI" things are NOT supported), will be ignored, and won't cause anything to break.

### Comments

Any lines starting with `# ` (hash sign followed by a space) will be ignored.

### Cash

Add some cash to your wallet:

```
<date> Add Cash <amount>

Example
2020/01/01 Add Cash $100
```

Pay for something with cash

```
<date> Pay Cash <amount> <category> <vendor>

Example
2020/01/01 Pay Cash $40 Restaurant BoilerRoom
```

Withdraw money from a bank account as cash

```
<date> Withdraw <amount> From <bank_name> Bank As Cash Fee <fee>

Example
2020/01/01 Withdraw $200 From Ally Bank As Cash Fee $3.25
```

### Credit cards

Add credit cards by name and starting balance:

```
<date> Add Card <name> <starting_balance>

Example
2020/01/01 Add Card Visa $12.10
```

_The starting balance is the amount owed at the beginning of records ($0 for a new card), not a credit limit. This tool doesn't have the capacity to track credit limits._

Make and refund charges on them:
```
<date> Charge <card_name> <amount> <category> <vendor_name>
<date> Refund <card_name> <amount> <category> <vendor_name>

Example
2020/01/02 Charge Visa $15.50 Restaurant CozyCorner
2020/01/03 Refund Visa $3.30 Restaurant CozyCorner
```

Close a credit card account (must have a balance of $0):

```
<date> Close <name> Card

Example
2020/01/04 Close Visa Card
```

### Banks

Add banks:

```
<date> Add Bank <name> <starting_balance>

Example
2020/01/01 Add Bank Ally $100
```

And deposit money into them:

```
<date> Deposit <amount> Into <bank_name> <category>

Example
2020/01/02 Deposit $12.50 Into Ally Gift
```

Pay a credit card balance from a bank:

```
<date> Pay <card_name> <amount> From <bank_name>

Example
2020/01/03 Pay Visa $10 From Ally
```

Close a bank account (must have a balance of $0):

```
<date> Close <name> Bank

Example
2020/01/04 Close Ally Bank
```

### Brokerages

Add a brokerage with a starting uninvested balance:

```
<date> Add Brokerage <name> <starting_balance>

Example
2020/01/02 Add Brokerage Etrade $230
```

Buy and sell stocks on a brokerage:

```
<date> Buy <count> Shares <symbol> On <brokerage_name> At <price_per_share>
<date> Sell <count> Shares <symbol> On <brokerage_name> At <price_per_share> Fee <fee_amount>

Example
2020/01/04 Buy 2 Shares BND On CompuShare At $10
2020/01/04 Sell 2 Shares BND On CompuShare At $11
```

Update the stock price (it can be psychologically harmful to do this too often - I would recommend once per month or less):

```
<date> Stock Price Of <symbol> Is <price>

Example
2020/01/10 Stock Price Of BND Is $15
```

(NYI) Buying shares at a given price should also update the price for any other instances of that stock held with that symbol, in the same brokerage or elsewhere.

Transfer money between a brokerage and a bank:

```
<date> Transfer <amount> From <bank_name> Bank To <brokerage_name> Brokerage
<date> Transfer <amount> From <brokerage_name> Brokerage To <bank_name> Bank

Examples
2020/01/01 Transfer $40 From Ally Bank To Etrade Brokerage
2020/01/02 Transfer $30 From Etrade Brokerage To Ally Bank
```

Deposit money into or deduct money from a brokerage

```
<date> Deposit <amount> Into <brokerage_name> Brokerage <category>
<date> Deduct <amount> From <brokerage_name> Brokerage <category>

Example
2020/01/01 Deposit $100 Into Etrade Brokerage Interest
2020/01/01 Deduct $10 From Etrade Brokerage Fees
```

### P2P's

That is - Peer to Peer payment methods, like Venmo or CashApp.

Add one:

```
<date> Add P2P <p2p_name> <starting_balance> Funded By <bank_name>

Example
2020/01/01 Add P2P Venmo $0 Funded By Ally
```

Change the funding bank:

```
<date> Change Funding For <p2p_name> To <bank_name>

Example
2020/01/04 Change Funding For Venmo To BOfA
```

Pay someone on said P2P:

```
<date> Pay <person> <amount> On <p2p_name> <category> <vendor>

Example
2020/01/02 Pay Alice $10 On Venmo Restaurant MySushi
```

_Note: If your are carrying enough balance on Venmo to fully fund the payment, then it comes out of your Venmo balance. Otherwise it'll come from whatever bank funds it._

Receive funds on Venmo:

```
<date> Receive <amount> On <p2p_name> From <person> <category> <vendor>

Example
2020/01/02 Receive $10 On Venmo From Alice Restaurant MySushi
```

_Note: Including the category and vendor is to help reconcile budgets, with the assumption that the reason that you're receiving the payment because you picked up the tab or something like that._

Transfer value from P2P to your bank.

```
<date> Transfer <amount> From P2P <p2p_name> To <bank_name> Bank

Example
2020/01/03 Transfer $10 From P2P Venmo To Ally Bank
```

### Splitwise

Create a new Splitwise group:

```
<date> Add Splitwise <group_name>

Example
2020/01/01 Add Splitwise MoneyFriends
```

Mark an expense that you paid on Splitwise:

```
<date> Splitwise Pay <amount> <group_name> <your_share> <category> <vendor>

Example with percent
2020/01/20 Splitwise Pay $30 MoneyFriends 50% InternetBill Comcast

Example with amount
2020/01/20 Splitwise Pay $30 MoneyFriends $15 InternetBill Comcast
```

Mark an expense that someone else paid on Splitwise:

```
<date> Splitwise Owe <amount> <group_name> <your_share> <category> <vendor>

Example with percent
2020/01/20 Splitwise Owe $30 MoneyFriends 50% InternetBill Comcast
```

`<your_share>` should be presented as a percentage.

If `<your_share>` is a dollar amount instead of a percentage, then drop the `<amount>` field entirely.

```
<date> Splitwise Owe <group_name> <your_share> <category> <vendor>

2020/01/20 Splitwise Owe MoneyFriends $15 InternetBill Comcast
```

Close a Splitwise account (must have a balance of $0):

```
<date> Close <group_name> Splitwise

Example
2020/01/04 Close MoneyFriends Splitwise
```

### Contributions

Add a contribution towards a category from a different category, usually from your paycheck:

```
<date> Contribute <amount> From <category1> To <category2>

Example
2020/01/01 Contribute $100 From Paycheck To Taxes
```

This is used to adjust budget amounts without affecting any financial instruments, so the above
example would cause your "Paycheck" budget to show $100 more and your "Taxes" budget to show $100
less (or an additional $100 spent).

## Output and analysis

### Cash

Simply show the amount of cash.

### Credit Cards

For each credit card, include its name and its out-standing balance:

```
Mastercard: $0.00
Visa: $17.60
```

### Banks

For each bank, include its name and current balance:

```
Ally: $650.34
TCF: $1100.00
```

### P2P's

Much like banks and credit cards, display a summary that includes each P2P and its corresponding balance:

```
Venmo: $100.10
CashApp: $14.67
```

### Splitwise

For each Splitwise group, show the current balance. If the amount owed is $0.00, and the group hasn't seen any activity within the last month, then it won't be included.

```
MoneyFriends: You owe $15.00
FunnyMends: You are owed $45.00
```

### Brokerages

For each brokerage, include its name, total current value (based on the last time the stocks owned were valued), and a summary of the assets held in the brokerage:

```
Etrade
 - Uninvested: $210.00
 - BND: 2 Shares ($30.00; $15.00 each)
```

If the uninvested balance is $0, then it won't show up.

### Portfolio Summary (NYI)

Show the total investment portfolio across all accounts - brokerages, retirement accounts, and a total amount of uninvested liquid assets.

```
Investments ($100.00)
 - BND: 10 Shares ($50.00; $5.00 each)
 - STK: 4 Shares ($50.00; $12.50 each)

Liquid Assets: $24.00
```

Liquid assets will be computed based on _all_ accounts that can hold a balance - bank accounts, cash, Venmo, Splitwise, etc.

### Budget Output

Show spending over the last several (up to 3 if available) months by category, sorted most-expensive category to least.

```
2020/01
  Paycheck: Received $1000
  Restaurants: Spent $100.00
  Streaming: Spent $42.00
```

This should take into account transactions across all different types of spending accounts - credit cards, Venmo, Splitwise, cash, direct bank transactions.

### Tax Output (NYI)

_This isn't well spec'ed out yet. It'll be easier to implement once it is!_

### Warning Output

It would probably be helpful to include a list of invalid commands, and also any commands that are partially valid but have some error, like trying to fund from a non-existing bank or charge a non-monetary amount. More details have yet to be spec'ed out.

### Ledger Files

Most types of accounts keep track of a ledger and a running balance. If you provide an output folder path, then Moneyfriend will write detailed ledger files. These can be helpful for reconciliation in the case of accounts, and for more detailed visibility into your spending, in the case of things like budgets.

```
$34.40
          +    $15.50    2020/01/02    TravelGasoline  Sunoco
$18.90
          +     $1.30    2020/01/02    Fun             FunShoppe
$17.60
          +     $5.50    2020/01/02    Restaurant      CozyCorner
$12.10
```

The following accounts are supported:

 - Bank accounts
 - Credit Cards
 - Brokerages
 - P2P's
 - Splitwise groups
 - Cash
 - Budgets
