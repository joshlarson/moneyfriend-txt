defmodule Splitwise do
  defstruct ledger: []

  def new(starting_balance) do
    %Splitwise{ledger: [starting_balance]}
  end

  def balance(%Splitwise{ledger: [balance | _]}) do
    balance
  end

  def pay(splitwise = %Splitwise{ledger: ledger}, transaction) do
    %Splitwise{splitwise | ledger: ledger |> Ledger.apply_transaction(transaction)}
  end

  def owe(splitwise = %Splitwise{ledger: ledger}, transaction) do
    %Splitwise{splitwise | ledger: ledger |> Ledger.apply_transaction(transaction)}
  end
end
