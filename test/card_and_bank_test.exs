defmodule CardAndBankTest do
  use ExUnit.Case
  doctest Moneyfriend

  describe "simple card setup" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Card Discover $0",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner",
            "2020/01/02 Charge Visa $1.30 Fun FunShoppe",
            "2020/01/02 Charge Visa $15.50 TravelGasolinePurchase Sunoco",
            "2020/01/03 Refund Visa $1.00 TravelGasolinePurchase Sunoco",
            ""
          ]
          |> Moneyfriend.process()
      }
    end

    test "has the right card balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Discover: $0.00",
                 "  Visa: $33.40"
               ]
    end

    test "has the right details for Visa", context do
      assert context[:friend] |> Moneyfriend.card_ledgers() |> Map.get("Visa") ==
               [
                 "$33.40",
                 "          -          $1.00    2020/01/03    TravelGasoline  Sunoco",
                 "$34.40",
                 "          +         $15.50    2020/01/02    TravelGasoline  Sunoco",
                 "$18.90",
                 "          +          $1.30    2020/01/02    Fun             FunShoppe",
                 "$17.60",
                 "          +          $5.50    2020/01/02    Restaurant      CozyCorner",
                 "$12.10"
               ]
    end

    test "has the right details for Discover", context do
      assert context[:friend] |> Moneyfriend.card_ledgers() |> Map.get("Discover") ==
               [
                 "$0.00"
               ]
    end

    test "budget ledgers show the correct grouping of transactions", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$5.50",
                 "          -          $5.50    2020/01/02    CozyCorner      Visa Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Fun") ==
               [
                 "-$1.30",
                 "          -          $1.30    2020/01/02    FunShoppe       Visa Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("TravelGasolinePurchase") ==
               [
                 "-$14.50",
                 "          +          $1.00    2020/01/03    Sunoco          Visa Card",
                 "-$15.50",
                 "          -         $15.50    2020/01/02    Sunoco          Visa Card",
                 "$0.00"
               ]
    end
  end

  describe "simple bank setup" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank TCF $100",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/02 Deposit $12.50 Into TCF Bank Gift Bob"
          ]
          |> Moneyfriend.process()
      }
    end

    test "has the right balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $352.00",
                 "  TCF: $112.50"
               ]
    end

    test "has the right details for Ally", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$352.00"
               ]
    end

    test "has the right details for TCF", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("TCF") ==
               [
                 "$112.50",
                 "          +         $12.50    2020/01/02    Gift            Bob",
                 "$100.00"
               ]
    end

    test "budget ledgers have an entry for the gift", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Gift") ==
               [
                 "$12.50",
                 "          +         $12.50    2020/01/02    Bob             TCF Bank",
                 "$0.00"
               ]
    end
  end

  describe "transfering money between bank accounts" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank TCF $100",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/02 Transfer $50 From TCF Bank To Ally Bank"
          ]
          |> Moneyfriend.process()
      }
    end

    test "has the right balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $402.00",
                 "  TCF: $50.00"
               ]
    end

    test "has the right details for Ally", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$402.00",
                 "          +         $50.00    2020/01/02    Transfer From   TCF Bank",
                 "$352.00"
               ]
    end

    test "has the right details for TCF", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("TCF") ==
               [
                 "$50.00",
                 "          -         $50.00    2020/01/02    Transfer To     Ally Bank",
                 "$100.00"
               ]
    end
  end

  describe "closing a bank account" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank TCF $100",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/02 Transfer $100 From TCF Bank To Ally Bank",
            "2020/01/03 Close TCF Bank"
          ]
          |> Moneyfriend.process()
      }
    end

    test "has the right balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $452.00"
               ]
    end

    test "does not have a ledger for TCF", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("TCF") == nil
    end

    test "has the right details for Ally", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$452.00",
                 "          +        $100.00    2020/01/02    Transfer From   TCF Bank",
                 "$352.00"
               ]
    end
  end

  describe "closing a bank account with a non-empty balance" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank TCF $100",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/03 Close TCF Bank"
          ]
          |> Moneyfriend.process()
      }
    end

    test "keeps the original balances and gets a warning", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $352.00",
                 "  TCF: $100.00",
                 "",
                 "Warnings",
                 "  Cannot close non-empty bank account 'TCF'"
               ]
    end

    test "keeps the ledger for TCF", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("TCF") ==
               [
                 "$100.00"
               ]
    end

    test "has the right details for Ally", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$352.00"
               ]
    end
  end

  describe "closing a bank account that does not exist" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank TCF $100",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/03 Close Blerg Bank"
          ]
          |> Moneyfriend.process()
      }
    end

    test "keeps the original balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $352.00",
                 "  TCF: $100.00",
                 "",
                 "Warnings",
                 "  Cannot close non-existent bank account 'Blerg'"
               ]
    end

    test "keeps the ledger for TCF", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("TCF") ==
               [
                 "$100.00"
               ]
    end

    test "has the right details for Ally", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$352.00"
               ]
    end
  end

  describe "making a card payment from a bank" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner",
            "2020/01/02 Deposit $12.50 Into Ally Bank Gift Bob",
            "2020/01/03 Pay Visa $10 From Ally"
          ]
          |> Moneyfriend.process()
      }
    end

    test "appropriately adjusts the card and bank balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $354.50",
                 "",
                 "Cards",
                 "  Visa: $7.60"
               ]
    end

    test "has the right details for Visa", context do
      assert context[:friend] |> Moneyfriend.card_ledgers() |> Map.get("Visa") ==
               [
                 "$7.60",
                 "          -         $10.00    2020/01/03    Payment From    Ally Bank",
                 "$17.60",
                 "          +          $5.50    2020/01/02    Restaurant      CozyCorner",
                 "$12.10"
               ]
    end

    test "has the right details for Ally", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$354.50",
                 "          -         $10.00    2020/01/03    Payment To      Visa",
                 "$364.50",
                 "          +         $12.50    2020/01/02    Gift            Bob",
                 "$352.00"
               ]
    end
  end

  describe "making a direct payment from a bank" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner",
            "2020/01/02 Deposit $12.50 Into Ally Bank Gift Bob",
            "2020/01/03 Pay $10 From Ally Bank Utilities ComEd"
          ]
          |> Moneyfriend.process()
      }
    end

    test "does not affect card or bank balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $354.50",
                 "",
                 "Cards",
                 "  Visa: $17.60"
               ]
    end

    test "has the right details for Ally", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$354.50",
                 "          -         $10.00    2020/01/03    Utilities       ComEd",
                 "$364.50",
                 "          +         $12.50    2020/01/02    Gift            Bob",
                 "$352.00"
               ]
    end

    test "is reflected in the budget ledgers", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Gift") ==
               [
                 "$12.50",
                 "          +         $12.50    2020/01/02    Bob             Ally Bank",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$5.50",
                 "          -          $5.50    2020/01/02    CozyCorner      Visa Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Utilities") ==
               [
                 "-$10.00",
                 "          -         $10.00    2020/01/03    ComEd           Ally Bank",
                 "$0.00"
               ]
    end
  end

  describe "invalid dollar amounts" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/02 Charge Visa lolz Restaurant CozyCorner",
            "2020/01/02 Deposit oops Into Ally Bank Gift Parents",
            "2020/01/03 Pay Visa $10 From Ally"
          ]
          |> Moneyfriend.process()
      }
    end

    test "don't impact the card or bank balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $342.00",
                 "",
                 "Cards",
                 "  Visa: $2.10",
                 "",
                 "Warnings",
                 "  Invalid Dollar Amount: 'lolz'",
                 "  Invalid Dollar Amount: 'oops'"
               ]
    end
  end

  describe "interacting with missing accounts" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Bank Ally $352",
            "2020/01/02 Charge Mastercard $5.50 Restaurant CozyCorner",
            "2020/01/02 Deposit $12.50 Into CapOne Bank Gift Parents",
            "2020/01/03 Pay Visa $10 From Ally"
          ]
          |> Moneyfriend.process()
      }
    end

    test "cause warnings", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $342.00",
                 "",
                 "Cards",
                 "  Visa: $2.10",
                 "",
                 "Warnings",
                 "  Missing Card: 'Mastercard'",
                 "  Missing Bank: 'CapOne'"
               ]
    end
  end

  describe "unrecognized commands" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Card Discover $0",
            "2020/01/02 Fooble Blorp",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner"
          ]
          |> Moneyfriend.process()
      }
    end

    test "are given warnings and do not affect the card balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Discover: $0.00",
                 "  Visa: $17.60",
                 "",
                 "Warnings",
                 "  Unrecognized Command: '2020/01/02 Fooble Blorp'"
               ]
    end
  end

  describe "comments" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Card Discover $0",
            "# 2020/01/01 Add Card Nope $0",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner"
          ]
          |> Moneyfriend.process()
      }
    end

    test "do not affect the balances and do not get warnings", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Discover: $0.00",
                 "  Visa: $17.60"
               ]
    end
  end

  describe "out-of-order dates" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/02 Add Card Discover $0",
            "2020/01/01 Charge Visa $5.50 Restaurant CozyCorner"
          ]
          |> Moneyfriend.process()
      }
    end

    test "cause a warning", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Discover: $0.00",
                 "  Visa: $17.60",
                 "",
                 "Warnings",
                 "  Out-of-order dates: '2020/01/01' should be before '2020/01/02'"
               ]
    end
  end

  describe "multiple out-of-order dates" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/02 Add Card Discover $0",
            "2020/01/01 Charge Visa $5.50 Restaurant CozyCorner",
            "2020/01/01 Charge Visa $4.50 Coffee FunCoffee"
          ]
          |> Moneyfriend.process()
      }
    end

    test "only warns about the first offender in a bunch", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Discover: $0.00",
                 "  Visa: $22.10",
                 "",
                 "Warnings",
                 "  Out-of-order dates: '2020/01/01' should be before '2020/01/02'"
               ]
    end
  end

  describe "multiple date-backtracks" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/02 Add Card Discover $0",
            "2020/01/01 Charge Visa $5.50 Restaurant CozyCorner",
            "2019/12/31 Charge Visa $4.50 Coffee FunCoffee"
          ]
          |> Moneyfriend.process()
      }
    end

    test "warns about each backtrack", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cards",
                 "  Discover: $0.00",
                 "  Visa: $22.10",
                 "",
                 "Warnings",
                 "  Out-of-order dates: '2020/01/01' should be before '2020/01/02'",
                 "  Out-of-order dates: '2019/12/31' should be before '2020/01/01'"
               ]
    end
  end
end
