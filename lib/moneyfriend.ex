defmodule Moneyfriend do
  def process(input) do
    input |> Enum.reduce(%Friend{}, &InputProcessor.apply_command_to_friend/2)
  end

  def summary(friend) do
    [
      summary_section("Cash", cash_summary(friend)),
      summary_section("Banks", bank_summary(friend)),
      summary_section("Cards", card_summary(friend)),
      summary_section("P2P's", p2p_summary(friend)),
      summary_section("Splitwises", splitwise_summary(friend)),
      summary_section("Brokerages", brokerage_summary(friend)),
      summary_section("Warnings", warning_summary(friend))
    ]
    |> Enum.reduce([], fn
      section, [] -> section
      [], summary -> summary
      section, summary -> Enum.concat(summary, ["" | section])
    end)
  end

  defp summary_section(title, contents) do
    case contents do
      [] -> []
      _ -> [title | contents |> Enum.map(&"  #{&1}")]
    end
  end

  defp card_summary(friend) do
    friend
    |> Friend.cards()
    |> Enum.map(&CardPrinter.summary/1)
  end

  def card_ledgers(friend) do
    friend
    |> Friend.cards()
    |> Enum.map(&CardPrinter.ledger/1)
    |> Map.new()
  end

  defp bank_summary(friend) do
    friend
    |> Friend.banks()
    |> Enum.map(&BankPrinter.summary/1)
  end

  def bank_ledgers(friend) do
    friend
    |> Friend.banks()
    |> Enum.map(&BankPrinter.ledger/1)
    |> Map.new()
  end

  defp cash_summary(friend) do
    friend |> Friend.cash() |> CashPrinter.summary()
  end

  def cash_ledger(friend) do
    friend |> Friend.cash() |> CashPrinter.ledger()
  end

  defp p2p_summary(friend) do
    friend
    |> Friend.p2ps()
    |> Enum.map(&P2PPrinter.summary/1)
  end

  def p2p_ledgers(friend) do
    friend
    |> Friend.p2ps()
    |> Enum.map(&P2PPrinter.ledger/1)
    |> Map.new()
  end

  defp splitwise_summary(friend) do
    friend
    |> Friend.splitwises()
    |> Enum.map(&SplitwisePrinter.summary/1)
  end

  def splitwise_ledgers(friend) do
    friend
    |> Friend.splitwises()
    |> Enum.map(&SplitwisePrinter.ledger/1)
    |> Map.new()
  end

  def budgets_by_month(friend) do
    friend
    |> Friend.budgets()
    |> Budgets.by_month()
    |> BudgetsPrinter.print_nested()
  end

  def budgets_by_year(friend) do
    friend
    |> Friend.budgets()
    |> Budgets.by_year()
    |> BudgetsPrinter.print_nested()
  end

  def budgets_by_category_per_month(friend) do
    friend
    |> Friend.budgets()
    |> Budgets.by_category_per_month()
    |> BudgetsPrinter.print_nested_reverse()
  end

  def budgets_by_category_per_year(friend) do
    friend
    |> Friend.budgets()
    |> Budgets.by_category_per_year()
    |> BudgetsPrinter.print_nested_reverse()
  end

  def budget_ledgers_by_month(friend) do
    friend
    |> Friend.budgets()
    |> Budgets.ledgers_by_month()
    |> BudgetsPrinter.print_ledgers()
  end

  def budget_ledgers_by_year(friend) do
    friend
    |> Friend.budgets()
    |> Budgets.ledgers_by_year()
    |> BudgetsPrinter.print_ledgers()
  end

  defp brokerage_summary(friend) do
    stock_ticker = friend |> Friend.stock_ticker()

    friend
    |> Friend.brokerage_snapshots()
    |> Enum.map(fn snapshot -> BrokerageSnapshotPrinter.summary(snapshot, stock_ticker) end)
  end

  def brokerage_long_summary(friend) do
    stock_ticker = friend |> Friend.stock_ticker()

    friend
    |> Friend.brokerage_snapshots()
    |> Enum.flat_map(fn snapshot ->
      BrokerageSnapshotPrinter.long_summary(snapshot, stock_ticker)
    end)
  end

  def brokerage_details(friend) do
    stock_ticker = friend |> Friend.stock_ticker()

    friend
    |> Friend.brokerages()
    |> Enum.map(fn entry -> BrokeragePrinter.details(entry, stock_ticker) end)
    |> Map.new()
  end

  def brokerage_balance_ratios_total(friend) do
    stock_ticker = friend |> Friend.stock_ticker()

    friend
    |> Friend.brokerage_snapshots()
    |> Enum.reduce(BrokerageSnapshot.new(), fn {_name, brokerage}, combined ->
      combined |> BrokerageSnapshot.combine(brokerage)
    end)
    |> BrokerageSnapshotPrinter.balance_ratios(stock_ticker)
  end

  defp warning_summary(%Friend{warnings: warnings}) do
    warnings
  end
end
