defmodule Friend do
  defstruct banks: %{},
            brokerages: %{},
            budgets: Budgets.new(),
            cards: %{},
            cash: Cash.new(),
            date: nil,
            p2ps: %{},
            splitwises: %{},
            stock_ticker: StockTicker.new(),
            warnings: []

  def cards(%Friend{cards: cards}) do
    cards |> Enum.sort()
  end

  def banks(%Friend{banks: banks}) do
    banks |> Enum.sort()
  end

  def cash(%Friend{cash: cash}) do
    cash
  end

  def p2ps(%Friend{p2ps: p2ps}) do
    p2ps |> Enum.sort()
  end

  def splitwises(%Friend{splitwises: splitwises}) do
    splitwises |> Enum.sort()
  end

  def budgets(%Friend{budgets: budgets}) do
    budgets
  end

  def brokerages(%Friend{brokerages: brokerages}) do
    brokerages |> Enum.sort()
  end

  def brokerage_snapshots(%Friend{brokerages: brokerages}) do
    brokerages
    |> Enum.sort()
    |> Enum.map(fn {name, brokerage} -> {name, BrokerageSnapshot.new(brokerage)} end)
  end

  def stock_ticker(%Friend{stock_ticker: stock_ticker}) do
    stock_ticker
  end

  def add_card(friend, card_name, starting_balance) do
    friend |> add_attr(:cards, card_name, Card.new(starting_balance))
  end

  def add_bank(friend, bank_name, starting_balance) do
    friend |> add_attr(:banks, bank_name, Bank.new(starting_balance))
  end

  def add_cash(friend = %Friend{cash: cash}, transaction) do
    %Friend{friend | cash: cash |> Cash.add(transaction)}
  end

  def add_p2p(friend, p2p_name, starting_balance, funding_bank_name) do
    friend |> add_attr(:p2ps, p2p_name, P2P.new(starting_balance, funding_bank_name))
  end

  def add_splitwise(friend, splitwise_name, starting_balance) do
    friend |> add_attr(:splitwises, splitwise_name, Splitwise.new(starting_balance))
  end

  def add_brokerage(friend, brokerage_name, balance) do
    friend |> add_attr(:brokerages, brokerage_name, Brokerage.new(balance))
  end

  def update_date(friend = %Friend{date: nil}, date) do
    friend |> Map.put(:date, date)
  end

  def update_date(friend = %Friend{date: previous_date}, date) do
    is_backtrack = previous_date > date && date != "#"

    cond do
      is_backtrack ->
        friend
        |> add_warning("Out-of-order dates: '#{date}' should be before '#{previous_date}'")

      true ->
        friend
    end
    |> Map.put(:date, date)
  end

  def add_warning(friend, message) do
    %Friend{friend | warnings: friend.warnings ++ [message]}
  end

  defp add_attr(friend, attr_type, attr_name, initial_value) do
    friend |> Map.update!(attr_type, &(&1 |> Map.put(attr_name, initial_value)))
  end

  def close_bank(friend = %Friend{banks: banks}, bank_name) do
    case banks do
      %{^bank_name => bank} ->
        case bank |> Bank.balance() do
          %Money{amount: 0} -> %Friend{friend | banks: banks |> Map.delete(bank_name)}
          _ -> friend |> add_warning("Cannot close non-empty bank account '#{bank_name}'")
        end

      %{} ->
        friend |> add_warning("Cannot close non-existent bank account '#{bank_name}'")
    end
  end

  def close_splitwise(friend = %Friend{splitwises: splitwises}, group_name) do
    case splitwises do
      %{^group_name => splitwise} ->
        case splitwise |> Splitwise.balance() do
          %Money{amount: 0} ->
            %Friend{friend | splitwises: splitwises |> Map.delete(group_name)}

          _ ->
            friend
            |> Friend.add_warning("Cannot close non-settled Splitwise group '#{group_name}'")
        end

      %{} ->
        friend |> Friend.add_warning("Cannot close non-existent Splitwise group '#{group_name}'")
    end
  end

  def update_budget(
        friend = %Friend{budgets: budgets},
        date,
        category,
        transaction = %Transaction{}
      ) do
    [year, month, _day] = date |> String.split("/")
    month_str = "#{year}/#{month}"

    new_budgets = budgets |> Budgets.update_budget(month_str, category, transaction)

    %Friend{friend | budgets: new_budgets}
  end

  def charge_card(friend, card_name, transaction) do
    friend |> update_attr(:cards, card_name, &Card.apply_transaction(&1, transaction))
  end

  def credit_card(friend, card_name, transaction) do
    friend |> update_attr(:cards, card_name, &Card.apply_transaction(&1, transaction))
  end

  def deposit(friend, bank_name, transaction) do
    friend |> update_attr(:banks, bank_name, &Bank.apply_transaction(&1, transaction))
  end

  def withdraw(friend, bank_name, transaction) do
    friend |> update_attr(:banks, bank_name, &Bank.apply_transaction(&1, transaction))
  end

  def pay_card_balance(
        friend,
        card_name,
        bank_name,
        transaction
      ) do
    friend
    |> withdraw(
      bank_name,
      transaction |> Transaction.for("Payment To", card_name) |> Transaction.reverse()
    )
    |> credit_card(
      card_name,
      transaction |> Transaction.for("Payment From", "#{bank_name} Bank") |> Transaction.reverse()
    )
  end

  def spend_cash(friend, transaction) do
    friend |> add_cash(Transaction.reverse(transaction))
  end

  def receive_p2p(friend, p2p_name, transaction = %Transaction{}) do
    friend
    |> update_attr(:p2ps, p2p_name, &P2P.receive(&1, transaction))
  end

  def pay_p2p(friend = %Friend{p2ps: p2ps}, p2p_name, transaction) do
    case p2ps |> Map.fetch(p2p_name) do
      {:ok, p2p} ->
        case p2p |> P2P.pay(transaction) do
          {:overdraft, bank_name, result} ->
            %Friend{friend | p2ps: p2ps |> Map.put(p2p_name, result)}
            |> withdraw(
              bank_name,
              transaction |> Transaction.for("P2P Charge", "#{p2p_name}") |> Transaction.reverse()
            )

          result ->
            %Friend{friend | p2ps: p2ps |> Map.put(p2p_name, result)}
        end

      :error ->
        friend |> add_warning("Missing P2P: '#{p2p_name}'")
    end
  end

  def update_p2p_funding(friend, p2p_name, bank_name) do
    friend
    |> update_attr(:p2ps, p2p_name, fn p2p ->
      p2p |> P2P.update_funding(bank_name)
    end)
  end

  def pay_splitwise(friend, group_name, transaction) do
    friend
    |> update_attr(:splitwises, group_name, fn splitwise ->
      splitwise |> Splitwise.pay(transaction)
    end)
  end

  def owe_splitwise(friend, group_name, transaction) do
    friend
    |> update_attr(:splitwises, group_name, fn splitwise ->
      splitwise |> Splitwise.owe(transaction |> Transaction.reverse())
    end)
  end

  def add_brokerage_value(friend, brokerage_name, transaction) do
    friend
    |> update_attr(:brokerages, brokerage_name, fn brokerage ->
      brokerage |> Brokerage.add_value(transaction)
    end)
  end

  def buy_stocks(friend, brokerage_name, symbol, stock_transaction, price) do
    friend
    |> update_attr(:brokerages, brokerage_name, fn brokerage ->
      brokerage |> Brokerage.buy_stocks(symbol, stock_transaction, price)
    end)
    |> update_stock_price(symbol, price)
  end

  def grant_stocks(friend, brokerage_name, symbol, count) do
    friend
    |> update_attr(:brokerages, brokerage_name, fn brokerage ->
      brokerage |> Brokerage.grant_stocks(symbol, count)
    end)
  end

  def update_stock_price(friend = %Friend{stock_ticker: stock_ticker}, symbol, price) do
    %Friend{friend | stock_ticker: stock_ticker |> StockTicker.update_stock_price(symbol, price)}
  end

  # Updates whichever attribute is specificed (by two keys, the attr_type, like :banks,
  # and the attr_name, like a particular bank name), by the function specified.
  defp update_attr(friend, attr_type, attr_name, func) do
    attr_collection = friend |> Map.get(attr_type)

    case attr_collection |> Map.fetch(attr_name) do
      {:ok, attr} ->
        case func.(attr) do
          result ->
            friend
            |> Map.put(attr_type, attr_collection |> Map.put(attr_name, result))
        end

      :error ->
        friend |> add_warning("Missing #{attr_type_printable(attr_type)}: '#{attr_name}'")
    end
  end

  defp attr_type_printable(:banks), do: "Bank"
  defp attr_type_printable(:cards), do: "Card"
  defp attr_type_printable(:brokerages), do: "Brokerage"
end
