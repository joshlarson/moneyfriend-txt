defmodule P2PTest do
  use ExUnit.Case
  doctest Moneyfriend

  describe "when receiving and paying money" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank Ally $230",
            "2020/01/01 Add Bank BOfA $500",
            "2020/01/02 Add P2P Venmo $50 Funded By Ally",
            "2020/01/02 Add P2P CashApp $61.82 Funded By Ally",
            "2020/01/03 Receive $41.50 On Venmo From Alice Restaurant CozyCorner",
            "2020/01/03 Pay Bob $11.82 On CashApp Entertainment LoganTheater"
          ]
          |> Moneyfriend.process()
      }
    end

    test "P2P balances are correctly adjusted and bank balances are unchanged", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $230.00",
                 "  BOfA: $500.00",
                 "",
                 "P2P's",
                 "  CashApp: $50.00",
                 "  Venmo: $91.50"
               ]
    end

    test "p2p ledgers include the right entries", context do
      assert context[:friend] |> Moneyfriend.p2p_ledgers() |> Map.get("Venmo") ==
               [
                 "$91.50",
                 "          +         $41.50    2020/01/03    Restaurant      CozyCorner / Alice",
                 "$50.00"
               ]

      assert context[:friend] |> Moneyfriend.p2p_ledgers() |> Map.get("CashApp") ==
               [
                 "$50.00",
                 "          -         $11.82    2020/01/03    Entertainment   LoganTheater / Bob",
                 "$61.82"
               ]
    end

    test "payments to and from are reflected in the budgets", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Entertainment") ==
               [
                 "-$11.82",
                 "          -         $11.82    2020/01/03    LoganTheater    CashApp / Bob",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "$41.50",
                 "          +         $41.50    2020/01/03    CozyCorner      Venmo / Alice",
                 "$0.00"
               ]
    end
  end

  describe "when paying more than the existing balance" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank Ally $230",
            "2020/01/01 Add Bank BOfA $500",
            "2020/01/02 Add P2P Venmo $0 Funded By Ally",
            "2020/01/02 Add P2P CashApp $10 Funded By BOfA",
            "2020/01/03 Pay Alice $41.50 On Venmo Restaurant CozyCorner",
            "2020/01/03 Pay Bob $11.82 On CashApp Entertainment LoganTheater"
          ]
          |> Moneyfriend.process()
      }
    end

    test "balance decreases come out of the funding banks, not the P2P's", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $188.50",
                 "  BOfA: $488.18",
                 "",
                 "P2P's",
                 "  CashApp: $10.00",
                 "  Venmo: $0.00"
               ]
    end

    test "p2p ledgers include the right entries", context do
      assert context[:friend] |> Moneyfriend.p2p_ledgers() |> Map.get("Venmo") ==
               [
                 "$0.00",
                 "          #          $0.00    2020/01/03    Restaurant      CozyCorner / Alice / $41.50 from Ally Bank",
                 "$0.00"
               ]

      assert context[:friend] |> Moneyfriend.p2p_ledgers() |> Map.get("CashApp") ==
               [
                 "$10.00",
                 "          #          $0.00    2020/01/03    Entertainment   LoganTheater / Bob / $11.82 from BOfA Bank",
                 "$10.00"
               ]
    end

    test "bank ledger includes an entry", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$188.50",
                 "          -         $41.50    2020/01/03    P2P Charge      Venmo",
                 "$230.00"
               ]

      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("BOfA") ==
               [
                 "$488.18",
                 "          -         $11.82    2020/01/03    P2P Charge      CashApp",
                 "$500.00"
               ]
    end
  end

  describe "when transferring money out of the P2P" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank Ally $230",
            "2020/01/02 Add P2P Venmo $45 Funded By Ally",
            "2020/01/03 Transfer $45 From P2P Venmo To Ally Bank"
          ]
          |> Moneyfriend.process()
      }
    end

    test "balance is moved from the P2P to the funding bank", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $275.00",
                 "",
                 "P2P's",
                 "  Venmo: $0.00"
               ]
    end

    test "p2p ledgers include the right entries", context do
      assert context[:friend] |> Moneyfriend.p2p_ledgers() |> Map.get("Venmo") ==
               [
                 "$0.00",
                 "          -         $45.00    2020/01/03    Transfer To     Ally Bank",
                 "$45.00"
               ]
    end

    test "bank ledger includes an entry", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$275.00",
                 "          +         $45.00    2020/01/03    Transfer From   Venmo",
                 "$230.00"
               ]
    end
  end

  describe "when changing the backing bank" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank Ally $500",
            "2020/01/01 Add Bank BOfA $500",
            "2020/01/02 Add P2P Venmo $0 Funded By Ally",
            "2020/01/03 Pay Alice $40 On Venmo Restaurant CozyCorner",
            "2020/01/04 Change Funding For Venmo To BOfA",
            "2020/01/05 Pay Bob $10 On Venmo Entertainment LoganTheater"
          ]
          |> Moneyfriend.process()
      }
    end

    test "balances are reduced from the correct funding banks", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $460.00",
                 "  BOfA: $490.00",
                 "",
                 "P2P's",
                 "  Venmo: $0.00"
               ]
    end

    test "p2p ledgers include the right entries", context do
      assert context[:friend] |> Moneyfriend.p2p_ledgers() |> Map.get("Venmo") ==
               [
                 "$0.00",
                 "          #          $0.00    2020/01/05    Entertainment   LoganTheater / Bob / $10.00 from BOfA Bank",
                 "$0.00",
                 "          #          $0.00    2020/01/03    Restaurant      CozyCorner / Alice / $40.00 from Ally Bank",
                 "$0.00"
               ]
    end

    test "bank ledgers each include the right entries", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$460.00",
                 "          -         $40.00    2020/01/03    P2P Charge      Venmo",
                 "$500.00"
               ]

      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("BOfA") ==
               [
                 "$490.00",
                 "          -         $10.00    2020/01/05    P2P Charge      Venmo",
                 "$500.00"
               ]
    end
  end
end
