defmodule BankPrinter do
  def summary({bank_name, bank}) do
    balance = Bank.balance(bank)
    "#{bank_name}: #{Money.to_string(balance)}"
  end

  def ledger({bank_name, %Bank{ledger: ledger}}) do
    {bank_name, LedgerPrinter.print_ledger(ledger)}
  end
end
