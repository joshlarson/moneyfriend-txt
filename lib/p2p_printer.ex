defmodule P2PPrinter do
  def summary({p2p_name, p2p}) do
    balance = P2P.balance(p2p)
    "#{p2p_name}: #{Money.to_string(balance)}"
  end

  def ledger({p2p_name, %P2P{ledger: ledger}}) do
    {p2p_name, LedgerPrinter.print_ledger(ledger)}
  end
end
