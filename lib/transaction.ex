defmodule Transaction do
  defstruct amount: Money.new(0, :USD), date: "", category: "", vendor: ""

  def of(amount, date) do
    %Transaction{amount: amount, date: date}
  end

  def for(transaction, category, vendor) do
    %Transaction{transaction | category: category, vendor: vendor}
  end

  def reverse(transaction = %Transaction{amount: amount}) do
    %Transaction{transaction | amount: Money.neg(amount)}
  end
end
