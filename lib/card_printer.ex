defmodule CardPrinter do
  def summary({card_name, card}) do
    balance = Card.balance(card)
    "#{card_name}: #{Money.to_string(balance)}"
  end

  def ledger({card_name, %Card{ledger: ledger}}) do
    {card_name, LedgerPrinter.print_ledger(ledger)}
  end
end
