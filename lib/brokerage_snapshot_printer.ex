defmodule BrokerageSnapshotPrinter do
  def summary({brokerage_name, snapshot = %BrokerageSnapshot{}}, stock_ticker) do
    uninvested_balance = snapshot |> BrokerageSnapshot.uninvested_balance()
    invested_balance = snapshot |> BrokerageSnapshot.invested_balance(stock_ticker)
    total_balance = Money.add(uninvested_balance, invested_balance)

    "#{brokerage_name}: #{total_balance} (#{uninvested_balance} uninvested, #{invested_balance} invested)"
  end

  def long_summary({brokerage_name, snapshot = %BrokerageSnapshot{}}, stock_ticker) do
    total_balance_str =
      snapshot |> BrokerageSnapshot.total_balance(stock_ticker) |> Money.to_string()

    summary_str = "#{brokerage_name}: #{total_balance_str}"

    uninvested_strs = snapshot |> BrokerageSnapshot.uninvested_balance() |> uninvested_strs()

    position_strs =
      snapshot
      |> BrokerageSnapshot.positions()
      |> Enum.map(fn args -> stock_str(args, stock_ticker) end)

    [summary_str | uninvested_strs ++ position_strs]
  end

  defp stock_str({symbol, stock_position}, stock_ticker) do
    count = stock_position |> StockPositionSnapshot.count() |> rounded_float()
    price = stock_ticker |> StockTicker.price_for(symbol)
    value = stock_position |> StockPositionSnapshot.value(price)
    " - #{symbol}: #{count} Shares (#{value}; #{price} each)"
  end

  defp uninvested_strs(%Money{amount: 0}) do
    []
  end

  defp uninvested_strs(balance) do
    [" - Uninvested: #{balance |> Money.to_string()}"]
  end

  def balance_ratios(brokerage = %BrokerageSnapshot{}, stock_ticker) do
    %Money{amount: total_value} = brokerage |> BrokerageSnapshot.total_balance(stock_ticker)
    uninvested_balance = brokerage |> BrokerageSnapshot.uninvested_balance()

    position_balances =
      brokerage
      |> BrokerageSnapshot.positions()
      |> Enum.map(fn {symbol, position} ->
        {symbol,
         position |> StockPositionSnapshot.value(stock_ticker |> StockTicker.price_for(symbol))}
      end)

    all_balances = [{"Uninvested", uninvested_balance} | position_balances]

    all_balances
    |> Enum.map(fn {name, %Money{amount: value}} -> {name, 100 * value / total_value} end)
    |> Enum.map(fn {name, percentage} ->
      "#{name}: #{percentage |> :erlang.float_to_binary(decimals: 1)}%"
    end)
  end

  defp rounded_float(count) do
    Float.floor(count + 0.0000005, 6)
  end
end
