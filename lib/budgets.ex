defmodule Budgets do
  @enforce_keys [:ledgers_by_month, :ledgers_by_year]
  defstruct [:ledgers_by_month, :ledgers_by_year]

  def new() do
    %Budgets{ledgers_by_month: %{}, ledgers_by_year: %{}}
  end

  def update_budget(
        %Budgets{ledgers_by_month: ledgers_by_month, ledgers_by_year: ledgers_by_year},
        month,
        category,
        transaction
      ) do
    year = month |> year_from_month()

    %Budgets{
      ledgers_by_month: ledgers_by_month |> add_transaction({month, category}, transaction),
      ledgers_by_year: ledgers_by_year |> add_transaction({year, category}, transaction)
    }
  end

  defp year_from_month(month) do
    [year, _] = month |> String.split("/")
    year
  end

  defp add_transaction(ledgers, key, transaction) do
    ledgers
    |> Map.put_new(key, Ledger.new())
    |> Map.update!(key, fn ledger -> ledger |> Ledger.apply_transaction(transaction) end)
  end

  def by_month(%Budgets{ledgers_by_month: ledgers}) do
    ledgers
    |> MapUtil.map_values(&Ledger.balance/1)
    |> MapUtil.expand_with_nested_keys()
  end

  def by_year(%Budgets{ledgers_by_year: ledgers}) do
    ledgers
    |> MapUtil.map_values(&Ledger.balance/1)
    |> MapUtil.expand_with_nested_keys()
  end

  def by_category_per_month(%Budgets{ledgers_by_month: ledgers}) do
    ledgers
    |> MapUtil.map_values(&Ledger.balance/1)
    |> MapUtil.map_keys(fn {month, category} -> {category, month} end)
    |> MapUtil.expand_with_nested_keys()
  end

  def by_category_per_year(%Budgets{ledgers_by_year: ledgers}) do
    ledgers
    |> MapUtil.map_values(&Ledger.balance/1)
    |> MapUtil.map_keys(fn {year, category} -> {category, year} end)
    |> MapUtil.expand_with_nested_keys()
  end

  def ledgers_by_month(%Budgets{ledgers_by_month: ledgers}) do
    ledgers |> MapUtil.expand_with_nested_keys()
  end

  def ledgers_by_year(%Budgets{ledgers_by_year: ledgers}) do
    ledgers |> MapUtil.expand_with_nested_keys()
  end
end
