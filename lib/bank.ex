defmodule Bank do
  defstruct ledger: Ledger.new()

  def new(starting_balance) do
    %Bank{ledger: Ledger.new(starting_balance)}
  end

  def balance(%Bank{ledger: ledger}) do
    Ledger.balance(ledger)
  end

  def apply_transaction(
        bank = %Bank{ledger: ledger},
        transaction = %Transaction{}
      ) do
    %Bank{bank | ledger: ledger |> Ledger.apply_transaction(transaction)}
  end
end
