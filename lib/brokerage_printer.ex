defmodule BrokeragePrinter do
  def details(
        {brokerage_name, brokerage = %Brokerage{uninvested_ledger: uninvested_ledger}},
        stock_ticker
      ) do
    positions = brokerage |> Brokerage.positions()

    ledgers =
      positions
      |> Enum.map(fn {symbol, stock_position} ->
        {symbol, stock_position_ledger_strs(stock_position)}
      end)
      |> Map.new()

    balance_ratios =
      brokerage
      |> BrokerageSnapshot.new()
      |> BrokerageSnapshotPrinter.balance_ratios(stock_ticker)

    long_summary =
      BrokerageSnapshotPrinter.long_summary(
        {brokerage_name, brokerage |> BrokerageSnapshot.new()},
        stock_ticker
      )

    {
      brokerage_name,
      ledgers
      |> Map.put("uninvested", LedgerPrinter.print_ledger(uninvested_ledger))
      |> Map.put("balance_ratios", balance_ratios)
      |> Map.put("summary", long_summary)
    }
  end

  defp stock_position_ledger_strs(%StockPosition{ledger: ledger}) do
    ledger |> Enum.map(&print_stock_ledger_item/1)
  end

  defp print_stock_ledger_item(%StockTransaction{
         count: count,
         date: date,
         description: description
       }) do
    {sign, display_count} =
      case count do
        count when count >= 0 -> {"+", count}
        count when count < 0 -> {"-", -count}
      end

    "          #{sign} #{Align.right("#{display_count |> rounded_float()}", 9)}    #{date}    #{description}"
  end

  defp print_stock_ledger_item(ledger_item) do
    "#{ledger_item |> rounded_float()}"
  end

  defp rounded_float(count) do
    Float.floor(count + 0.0000005, 6)
  end
end
