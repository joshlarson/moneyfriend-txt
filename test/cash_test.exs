defmodule CashTest do
  use ExUnit.Case
  doctest Moneyfriend

  describe "when cash does not exist" do
    setup do
      {
        :ok,
        friend: [] |> Moneyfriend.process()
      }
    end

    test "does not have a cash summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               []
    end

    test "does not have a ledger", context do
      assert context[:friend] |> Moneyfriend.cash_ledger() ==
               [
                 "$0.00"
               ]
    end
  end

  describe "when cash exists" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Cash $100",
            "2020/01/02 Add Cash $25"
          ]
          |> Moneyfriend.process()
      }
    end

    test "has a cash summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cash",
                 "  $125.00"
               ]
    end

    test "has a ledger", context do
      assert context[:friend] |> Moneyfriend.cash_ledger() ==
               [
                 "$125.00",
                 "          +         $25.00    2020/01/02    Add             ",
                 "$100.00",
                 "          +        $100.00    2020/01/01    Add             ",
                 "$0.00"
               ]
    end
  end

  describe "when withdrawing from a bank" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank Ally $100",
            "2020/01/01 Withdraw $50 From Ally Bank As Cash Fee $3.25"
          ]
          |> Moneyfriend.process()
      }
    end

    test "cash is increased and bank balance is decreased by the amount withdrawn", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cash",
                 "  $50.00",
                 "",
                 "Banks",
                 "  Ally: $46.75"
               ]
    end

    test "cash ledger shows the withdrawal", context do
      assert context[:friend] |> Moneyfriend.cash_ledger() ==
               [
                 "$50.00",
                 "          +         $50.00    2020/01/01    ATM Withdrawal  From Ally Bank",
                 "$0.00"
               ]
    end

    test "bank ledger includes entries for withdrawal and for fee", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$46.75",
                 "          -          $3.25    2020/01/01    Fee             ATM",
                 "$50.00",
                 "          -         $50.00    2020/01/01    Withdrawal As   Cash",
                 "$100.00"
               ]
    end

    test "the fee is reflected in the budget ledgers", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Fee") ==
               [
                 "-$3.25",
                 "          -          $3.25    2020/01/01    ATM Fee At      Ally Bank",
                 "$0.00"
               ]
    end
  end

  describe "when withdrawing from a bank (and calling the fee something different)" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank Ally $100",
            "2020/01/01 Withdraw $50 From Ally Bank As Cash AtmFee $3.25"
          ]
          |> Moneyfriend.process()
      }
    end

    test "cash is increased and bank balance is decreased by the amount withdrawn", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cash",
                 "  $50.00",
                 "",
                 "Banks",
                 "  Ally: $46.75"
               ]
    end

    test "ledger shows the withdrawal", context do
      assert context[:friend] |> Moneyfriend.cash_ledger() ==
               [
                 "$50.00",
                 "          +         $50.00    2020/01/01    ATM Withdrawal  From Ally Bank",
                 "$0.00"
               ]
    end

    test "bank_ledger includes entries for withdrawal and for fee", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$46.75",
                 "          -          $3.25    2020/01/01    Fee             ATM",
                 "$50.00",
                 "          -         $50.00    2020/01/01    Withdrawal As   Cash",
                 "$100.00"
               ]
    end
  end

  describe "when paying for something in cash" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Cash $100",
            "2020/01/02 Pay Cash $40 Restaurant BoilerRoom",
            "2020/01/03 Receive Cash $10 Restaurant BoilerRoom"
          ]
          |> Moneyfriend.process()
      }
    end

    test "cash is decreased by the amount spent", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Cash",
                 "  $70.00"
               ]
    end

    test "ledger shows the charge", context do
      assert context[:friend] |> Moneyfriend.cash_ledger() ==
               [
                 "$70.00",
                 "          +         $10.00    2020/01/03    Restaurant      BoilerRoom",
                 "$60.00",
                 "          -         $40.00    2020/01/02    Restaurant      BoilerRoom",
                 "$100.00",
                 "          +        $100.00    2020/01/01    Add             ",
                 "$0.00"
               ]
    end

    test "is reflected in the budget ledgers", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$30.00",
                 "          +         $10.00    2020/01/03    BoilerRoom      Cash",
                 "-$40.00",
                 "          -         $40.00    2020/01/02    BoilerRoom      Cash",
                 "$0.00"
               ]
    end
  end
end
