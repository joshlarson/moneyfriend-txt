defmodule BrokerageTest do
  use ExUnit.Case
  doctest Moneyfriend

  describe "with a deposit into a brokerage and a fee" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $0",
            "2020/01/02 Deposit $100 Into CompuShare Brokerage SignupBonus",
            "2020/01/03 Deduct $10 From CompuShare Brokerage Fees"
          ]
          |> Moneyfriend.process()
      }
    end

    test "brokerage balance is increased", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  CompuShare: $90.00 ($90.00 uninvested, $0.00 invested)"
               ]
    end

    test "balance and uninvested balance are shown on the long summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "CompuShare: $90.00",
                 " - Uninvested: $90.00"
               ]
    end

    test "brokerage uninvested ledger has the right entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$90.00",
                 "          -         $10.00    2020/01/03    Deduction       Fees",
                 "$100.00",
                 "          +        $100.00    2020/01/02    Deposit         SignupBonus",
                 "$0.00"
               ]
    end

    test "brokerage details includes a balance ratio entry", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 100.0%"
               ]
    end

    test "there is a budget item for brokerage deposits", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("SignupBonus") ==
               [
                 "$100.00",
                 "          +        $100.00    2020/01/02    Deposit Into    CompuShare",
                 "$0.00"
               ]
    end

    test "there is a budget item for fees", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Fees") ==
               [
                 "-$10.00",
                 "          -         $10.00    2020/01/03    Deduction From  CompuShare",
                 "$0.00"
               ]
    end

    test "brokerage balance ratios do not exist", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_balance_ratios_total() ==
               [
                 "Uninvested: 100.0%"
               ]
    end
  end

  describe "with transfers between a bank and a brokerage" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $0",
            "2020/01/02 Add Bank Ally $250",
            "2020/01/02 Transfer $100 From Ally Bank To CompuShare Brokerage",
            "2020/01/03 Transfer $25 From CompuShare Brokerage To Ally Bank"
          ]
          |> Moneyfriend.process()
      }
    end

    test "balance is moved from bank to brokerage", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Banks",
                 "  Ally: $175.00",
                 "",
                 "Brokerages",
                 "  CompuShare: $75.00 ($75.00 uninvested, $0.00 invested)"
               ]
    end

    test "balances are shown on the long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "CompuShare: $75.00",
                 " - Uninvested: $75.00"
               ]
    end

    test "brokerage uninvested ledger has the right entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$75.00",
                 "          -         $25.00    2020/01/03    Transfer To     Ally Bank",
                 "$100.00",
                 "          +        $100.00    2020/01/02    Transfer From   Ally Bank",
                 "$0.00"
               ]
    end

    test "bank ledger has entries", context do
      assert context[:friend] |> Moneyfriend.bank_ledgers() |> Map.get("Ally") ==
               [
                 "$175.00",
                 "          +         $25.00    2020/01/03    Transfer From   CompuShare",
                 "$150.00",
                 "          -        $100.00    2020/01/02    Transfer To     CompuShare",
                 "$250.00"
               ]
    end
  end

  describe "with some stock purchases in one brokerage" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $100",
            "2020/01/04 Buy 2 Shares BND On CompuShare At $10",
            "2020/01/07 Buy 3 Shares BND On CompuShare At $10"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows invested and uninvested balances", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  CompuShare: $100.00 ($50.00 uninvested, $50.00 invested)"
               ]
    end

    test "shows stocks in the long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "CompuShare: $100.00",
                 " - Uninvested: $50.00",
                 " - BND: 5.0 Shares ($50.00; $10.00 each)"
               ]
    end

    test "brokerage uninvested ledger has the right entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$50.00",
                 "          -         $30.00    2020/01/07    Buy             3.0 Shares BND",
                 "$80.00",
                 "          -         $20.00    2020/01/04    Buy             2.0 Shares BND",
                 "$100.00"
               ]
    end

    test "brokerage details includes a balance ratio", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 50.0%",
                 "BND: 50.0%"
               ]
    end

    test "stock ledgers have entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("BND") ==
               [
                 "5.0",
                 "          +       3.0    2020/01/07    Buy",
                 "2.0",
                 "          +       2.0    2020/01/04    Buy",
                 "0.0"
               ]
    end

    test "brokerage balance ratios are correct", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_balance_ratios_total() ==
               [
                 "Uninvested: 50.0%",
                 "BND: 50.0%"
               ]
    end
  end

  describe "with some stock purchases and sales in multiple brokerages" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $150",
            "2020/01/02 Add Brokerage Ally $230",
            "2020/01/04 Buy 2 Shares BND On CompuShare At $10",
            "2020/01/04 Buy 5 Shares VTI On Ally At $20",
            "2020/01/05 Buy 25 Shares VXUS On Ally At $5",
            "2020/01/07 Buy 3 Shares BND On CompuShare At $10",
            "2020/01/08 Sell 5 Shares VXUS On Ally At $7"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows balances across different brokerages", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  Ally: $280.00 ($40.00 uninvested, $240.00 invested)",
                 "  CompuShare: $150.00 ($100.00 uninvested, $50.00 invested)"
               ]
    end

    test "shows stocks aggregated by brokerage", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "Ally: $280.00",
                 " - Uninvested: $40.00",
                 " - VTI: 5.0 Shares ($100.00; $20.00 each)",
                 " - VXUS: 20.0 Shares ($140.00; $7.00 each)",
                 "CompuShare: $150.00",
                 " - Uninvested: $100.00",
                 " - BND: 5.0 Shares ($50.00; $10.00 each)"
               ]
    end

    test "brokerage uninvested ledger has the right entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$100.00",
                 "          -         $30.00    2020/01/07    Buy             3.0 Shares BND",
                 "$130.00",
                 "          -         $20.00    2020/01/04    Buy             2.0 Shares BND",
                 "$150.00"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("uninvested") ==
               [
                 "$40.00",
                 "          +         $35.00    2020/01/08    Sell            5.0 Shares VXUS",
                 "$5.00",
                 "          -        $125.00    2020/01/05    Buy             25.0 Shares VXUS",
                 "$130.00",
                 "          -        $100.00    2020/01/04    Buy             5.0 Shares VTI",
                 "$230.00"
               ]
    end

    test "brokerage details includes a balance ratio", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 66.7%",
                 "BND: 33.3%"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 14.3%",
                 "VTI: 35.7%",
                 "VXUS: 50.0%"
               ]
    end

    test "stock ledgers have entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("BND") ==
               [
                 "5.0",
                 "          +       3.0    2020/01/07    Buy",
                 "2.0",
                 "          +       2.0    2020/01/04    Buy",
                 "0.0"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("VTI") ==
               [
                 "5.0",
                 "          +       5.0    2020/01/04    Buy",
                 "0.0"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("VXUS") ==
               [
                 "20.0",
                 "          -       5.0    2020/01/08    Sell",
                 "25.0",
                 "          +      25.0    2020/01/05    Buy",
                 "0.0"
               ]
    end

    test "brokerage balance ratios are correct", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_balance_ratios_total() ==
               [
                 "Uninvested: 32.6%",
                 "BND: 11.6%",
                 "VTI: 23.3%",
                 "VXUS: 32.6%"
               ]
    end
  end

  describe "with multiple brokerages that own some overlapping stocks" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $150",
            "2020/01/02 Add Brokerage Ally $230",
            "2020/01/04 Buy 2 Shares BND On CompuShare At $10",
            "2020/01/04 Buy 5 Shares VTI On Ally At $20",
            "2020/01/05 Buy 15 Shares VXUS On Ally At $5",
            "2020/01/07 Buy 3 Shares BND On CompuShare At $10",
            "2020/01/08 Buy 10 Shares VXUS On CompuShare At $5",
            "2020/01/09 Sell 5 Shares VXUS On Ally At $7"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows updated balances in all brokerages", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  Ally: $260.00 ($90.00 uninvested, $170.00 invested)",
                 "  CompuShare: $170.00 ($50.00 uninvested, $120.00 invested)"
               ]
    end

    test "shows prices updated across all brokerages", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "Ally: $260.00",
                 " - Uninvested: $90.00",
                 " - VTI: 5.0 Shares ($100.00; $20.00 each)",
                 " - VXUS: 10.0 Shares ($70.00; $7.00 each)",
                 "CompuShare: $170.00",
                 " - Uninvested: $50.00",
                 " - BND: 5.0 Shares ($50.00; $10.00 each)",
                 " - VXUS: 10.0 Shares ($70.00; $7.00 each)"
               ]
    end

    test "brokerage balance ratios are correct", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_balance_ratios_total() ==
               [
                 "Uninvested: 32.6%",
                 "BND: 11.6%",
                 "VTI: 23.3%",
                 "VXUS: 32.6%"
               ]
    end
  end

  describe "with transfers between brokerages" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $150",
            "2020/01/02 Add Brokerage Ally $230",
            "2020/01/04 Transfer $20 From Ally Brokerage To CompuShare Brokerage"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows the balance transferred to the correct brokerage", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  Ally: $210.00 ($210.00 uninvested, $0.00 invested)",
                 "  CompuShare: $170.00 ($170.00 uninvested, $0.00 invested)"
               ]
    end

    test "shows the correct long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "Ally: $210.00",
                 " - Uninvested: $210.00",
                 "CompuShare: $170.00",
                 " - Uninvested: $170.00"
               ]
    end

    test "brokerage uninvested ledgers have the right entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$170.00",
                 "          +         $20.00    2020/01/04    Transfer From   Ally Brokerage",
                 "$150.00"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("uninvested") ==
               [
                 "$210.00",
                 "          -         $20.00    2020/01/04    Transfer To     CompuShare Brokerage",
                 "$230.00"
               ]
    end
  end

  describe "with all of the balance invested" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $150",
            "2020/01/02 Add Brokerage Ally $230",
            "2020/01/04 Buy 2 Shares BND On CompuShare At $10",
            "2020/01/04 Buy 5 Shares VTI On Ally At $20",
            "2020/01/05 Buy 26 Shares VXUS On Ally At $5"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows the zero uninvested amount in the summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  Ally: $230.00 ($0.00 uninvested, $230.00 invested)",
                 "  CompuShare: $150.00 ($130.00 uninvested, $20.00 invested)"
               ]
    end

    test "does not show an uninvested amount in the long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "Ally: $230.00",
                 " - VTI: 5.0 Shares ($100.00; $20.00 each)",
                 " - VXUS: 26.0 Shares ($130.00; $5.00 each)",
                 "CompuShare: $150.00",
                 " - Uninvested: $130.00",
                 " - BND: 2.0 Shares ($20.00; $10.00 each)"
               ]
    end

    test "balance ratios do show an uninvested amount", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 86.7%",
                 "BND: 13.3%"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 0.0%",
                 "VTI: 43.5%",
                 "VXUS: 56.5%"
               ]
    end

    test "brokerage uninvested ledgers have the right entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("uninvested") ==
               [
                 "$0.00",
                 "          -        $130.00    2020/01/05    Buy             26.0 Shares VXUS",
                 "$130.00",
                 "          -        $100.00    2020/01/04    Buy             5.0 Shares VTI",
                 "$230.00"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$130.00",
                 "          -         $20.00    2020/01/04    Buy             2.0 Shares BND",
                 "$150.00"
               ]
    end
  end

  describe "if the price of a stock changes" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $150",
            "2020/01/02 Add Brokerage Ally $230",
            "2020/01/04 Buy 2 Shares BND On CompuShare At $10",
            "2020/01/04 Buy 5 Shares VTI On Ally At $20",
            "2020/01/05 Buy 26 Shares VXUS On Ally At $5",
            "2020/01/10 Stock Price Of BND Is $15"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows an uninvested amount in the summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  Ally: $230.00 ($0.00 uninvested, $230.00 invested)",
                 "  CompuShare: $160.00 ($130.00 uninvested, $30.00 invested)"
               ]
    end

    test "does not show an uninvested amount in the long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "Ally: $230.00",
                 " - VTI: 5.0 Shares ($100.00; $20.00 each)",
                 " - VXUS: 26.0 Shares ($130.00; $5.00 each)",
                 "CompuShare: $160.00",
                 " - Uninvested: $130.00",
                 " - BND: 2.0 Shares ($30.00; $15.00 each)"
               ]
    end

    test "balance ratios are based on the updated price", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 81.3%",
                 "BND: 18.8%"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 0.0%",
                 "VTI: 43.5%",
                 "VXUS: 56.5%"
               ]
    end

    test "brokerage balance ratios are correct", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_balance_ratios_total() ==
               [
                 "Uninvested: 33.3%",
                 "BND: 7.7%",
                 "VTI: 25.6%",
                 "VXUS: 33.3%"
               ]
    end
  end

  describe "with some stock grants and price changes" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $150",
            "2020/01/02 Add Brokerage Ally $230",
            "2020/01/04 Grant 2 Shares BND On CompuShare",
            "2020/01/04 Grant 5 Shares VTI On Ally",
            "2020/01/05 Grant 25 Shares VXUS On Ally",
            "2020/01/05 Stock Price Of BND Is $10",
            "2020/01/05 Stock Price Of VTI Is $20",
            "2020/01/05 Stock Price Of VXUS Is $5"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows updated prices in the summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  Ally: $455.00 ($230.00 uninvested, $225.00 invested)",
                 "  CompuShare: $170.00 ($150.00 uninvested, $20.00 invested)"
               ]
    end

    test "shows stocks aggregated by brokerage in the long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "Ally: $455.00",
                 " - Uninvested: $230.00",
                 " - VTI: 5.0 Shares ($100.00; $20.00 each)",
                 " - VXUS: 25.0 Shares ($125.00; $5.00 each)",
                 "CompuShare: $170.00",
                 " - Uninvested: $150.00",
                 " - BND: 2.0 Shares ($20.00; $10.00 each)"
               ]
    end

    test "uninvested ledger does not have entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$150.00"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("uninvested") ==
               [
                 "$230.00"
               ]
    end

    test "stock ledgers have entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("BND") ==
               [
                 "2.0",
                 "          +       2.0    2020/01/04    Grant",
                 "0.0"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("VTI") ==
               [
                 "5.0",
                 "          +       5.0    2020/01/04    Grant",
                 "0.0"
               ]

      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("Ally")
             |> Map.get("VXUS") ==
               [
                 "25.0",
                 "          +      25.0    2020/01/05    Grant",
                 "0.0"
               ]
    end
  end

  describe "if the price of a stock changes and then you buy stock afterwards" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $150",
            "2020/01/02 Add Brokerage Ally $230",
            "2020/01/04 Buy 2 Shares BND On CompuShare At $10",
            "2020/01/04 Buy 5 Shares VTI On Ally At $20",
            "2020/01/05 Buy 26 Shares VXUS On Ally At $5",
            "2020/01/10 Stock Price Of BND Is $15",
            "2020/01/15 Buy 2 Shares PYPL On CompuShare At $5"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows an uninvested amount in the summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  Ally: $230.00 ($0.00 uninvested, $230.00 invested)",
                 "  CompuShare: $160.00 ($120.00 uninvested, $40.00 invested)"
               ]
    end

    test "does not show an uninvested amount in the long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "Ally: $230.00",
                 " - VTI: 5.0 Shares ($100.00; $20.00 each)",
                 " - VXUS: 26.0 Shares ($130.00; $5.00 each)",
                 "CompuShare: $160.00",
                 " - Uninvested: $120.00",
                 " - BND: 2.0 Shares ($30.00; $15.00 each)",
                 " - PYPL: 2.0 Shares ($10.00; $5.00 each)"
               ]
    end
  end

  describe "with stock purchases of fractional shares" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Brokerage CompuShare $100",
            "2020/01/04 Buy 2.5 Shares BND On CompuShare At $10",
            "2020/01/07 Buy 3 Shares BND On CompuShare At $10"
          ]
          |> Moneyfriend.process()
      }
    end

    test "shows accurate balances in the summary", context do
      assert context[:friend] |> Moneyfriend.summary() ==
               [
                 "Brokerages",
                 "  CompuShare: $100.00 ($45.00 uninvested, $55.00 invested)"
               ]
    end

    test "shows accurate balances in the long_summary", context do
      assert context[:friend] |> Moneyfriend.brokerage_long_summary() ==
               [
                 "CompuShare: $100.00",
                 " - Uninvested: $45.00",
                 " - BND: 5.5 Shares ($55.00; $10.00 each)"
               ]
    end

    test "brokerage uninvested ledger has the right entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("uninvested") ==
               [
                 "$45.00",
                 "          -         $30.00    2020/01/07    Buy             3.0 Shares BND",
                 "$75.00",
                 "          -         $25.00    2020/01/04    Buy             2.5 Shares BND",
                 "$100.00"
               ]
    end

    test "brokerage details includes a balance ratio", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("balance_ratios") ==
               [
                 "Uninvested: 45.0%",
                 "BND: 55.0%"
               ]
    end

    test "stock ledgers have entries", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_details()
             |> Map.get("CompuShare")
             |> Map.get("BND") ==
               [
                 "5.5",
                 "          +       3.0    2020/01/07    Buy",
                 "2.5",
                 "          +       2.5    2020/01/04    Buy",
                 "0.0"
               ]
    end

    test "brokerage balance ratios are correct", context do
      assert context[:friend]
             |> Moneyfriend.brokerage_balance_ratios_total() ==
               [
                 "Uninvested: 45.0%",
                 "BND: 55.0%"
               ]
    end
  end
end
