defmodule Align do
  def right(str, size) do
    length = String.length(str)
    spaces = String.duplicate(" ", size - length)
    "#{spaces}#{str}"
  end

  def left(str, size) do
    length = String.length(str)

    if length <= size do
      spaces = String.duplicate(" ", size - length)
      "#{str}#{spaces}"
    else
      str |> String.slice(0, size)
    end
  end
end
