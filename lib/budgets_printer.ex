defmodule BudgetsPrinter do
  def print_nested(budget_balances) do
    budget_balances
    |> MapUtil.map_values(fn arr ->
      arr
      |> Enum.map(fn {category, balance} ->
        "#{category}: #{balance |> Money.to_string()}"
      end)
      |> Enum.sort()
    end)
  end

  def print_nested_reverse(budget_balances) do
    budget_balances
    |> print_nested()
    |> MapUtil.map_values(&Enum.reverse/1)
  end

  def print_ledgers(budget_ledgers) do
    budget_ledgers
    |> MapUtil.map_values(fn arr ->
      arr
      |> MapUtil.map_values(&LedgerPrinter.print_ledger/1)
    end)
  end
end
