defmodule Cash do
  defstruct ledger: Ledger.new()

  def new() do
    %Cash{ledger: Ledger.new()}
  end

  def balance(%Cash{ledger: ledger}) do
    Ledger.balance(ledger)
  end

  def add(%Cash{ledger: ledger}, transaction) do
    %Cash{
      ledger: ledger |> Ledger.apply_transaction(transaction)
    }
  end
end
