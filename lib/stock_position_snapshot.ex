defmodule StockPositionSnapshot do
  @enforce_keys [:count]
  defstruct [:count]

  def new(position = %StockPosition{}) do
    %StockPositionSnapshot{count: position |> StockPosition.count()}
  end

  def combine(%StockPositionSnapshot{count: count1}, %StockPositionSnapshot{count: count2}) do
    %StockPositionSnapshot{count: count1 + count2}
  end

  def value(%StockPositionSnapshot{count: count}, price) do
    Money.multiply(price, count)
  end

  def count(%StockPositionSnapshot{count: count}) do
    count
  end
end
