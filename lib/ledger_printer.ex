defmodule LedgerPrinter do
  def print_ledger(ledger) do
    ledger |> Enum.map(&print_ledger_item/1)
  end

  def print_ledger_item(item = %Money{}) do
    Money.to_string(item)
  end

  def print_ledger_item(%Transaction{
        amount: amount,
        date: date,
        category: category,
        vendor: vendor
      }) do
    "          #{format_amount(amount)}    #{date}    #{Align.left(category, 14)}  #{vendor}"
  end

  defp format_amount(amount) do
    {sign, amount} =
      case amount do
        %Money{amount: cents} when cents == 0 -> {"#", amount}
        %Money{amount: cents} when cents > 0 -> {"+", amount}
        %Money{amount: cents} when cents < 0 -> {"-", Money.neg(amount)}
      end

    aligned_amount_str = Align.right(Money.to_string(amount), 15)
    "#{sign}#{aligned_amount_str}"
  end
end
