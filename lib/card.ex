defmodule Card do
  defstruct ledger: Ledger.new()

  def new(starting_balance) do
    %Card{ledger: Ledger.new(starting_balance)}
  end

  def balance(%Card{ledger: ledger}) do
    Ledger.balance(ledger)
  end

  def apply_transaction(
        card = %Card{ledger: ledger},
        transaction = %Transaction{}
      ) do
    %Card{card | ledger: ledger |> Ledger.apply_transaction(transaction)}
  end
end
