defmodule Brokerage do
  defstruct balance: Money.new(0, :USD),
            uninvested_ledger: Ledger.new(),
            positions: %{}

  def new(balance) do
    %Brokerage{balance: balance, uninvested_ledger: Ledger.new(balance)}
  end

  def add_value(
        brokerage = %Brokerage{balance: balance, uninvested_ledger: ledger},
        transaction = %Transaction{amount: amount}
      ) do
    new_ledger = ledger |> Ledger.apply_transaction(transaction)
    %Brokerage{brokerage | balance: Money.add(balance, amount), uninvested_ledger: new_ledger}
  end

  def grant_stocks(
        brokerage = %Brokerage{positions: positions},
        symbol,
        count
      ) do
    old_position = positions |> Map.get(symbol, %StockPosition{})

    new_position =
      old_position
      |> StockPosition.add_shares(count)

    new_positions = positions |> Map.put(symbol, new_position)

    %{brokerage | positions: new_positions}
  end

  def buy_stocks(
        brokerage = %Brokerage{
          balance: balance,
          positions: positions,
          uninvested_ledger: uninvested_ledger
        },
        symbol,
        stock_transaction = %StockTransaction{count: count},
        price
      ) do
    old_position = positions |> Map.get(symbol, %StockPosition{})

    new_position =
      old_position
      |> StockPosition.add_shares(stock_transaction)

    new_positions = positions |> Map.put(symbol, new_position)

    cost = Money.multiply(price, count)
    new_balance = Money.subtract(balance, cost)

    transaction =
      if count > 0 do
        Transaction.of(cost, stock_transaction.date)
        |> Transaction.for("Buy", "#{count} Shares #{symbol}")
        |> Transaction.reverse()
      else
        Transaction.of(cost, stock_transaction.date)
        |> Transaction.for("Sell", "#{-count} Shares #{symbol}")
        |> Transaction.reverse()
      end

    %{
      brokerage
      | balance: new_balance,
        positions: new_positions,
        uninvested_ledger: uninvested_ledger |> Ledger.apply_transaction(transaction)
    }
  end

  def uninvested_balance(%Brokerage{balance: balance}) do
    balance
  end

  def positions(%Brokerage{positions: positions}) do
    positions |> Enum.sort()
  end
end
