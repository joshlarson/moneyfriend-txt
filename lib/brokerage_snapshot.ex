defmodule BrokerageSnapshot do
  @enforce_keys [:uninvested_balance, :positions]
  defstruct [:uninvested_balance, :positions]

  def new() do
    %BrokerageSnapshot{uninvested_balance: Money.new(0, :USD), positions: []}
  end

  def new(brokerage) do
    %BrokerageSnapshot{
      uninvested_balance: brokerage |> Brokerage.uninvested_balance(),
      positions:
        brokerage
        |> Brokerage.positions()
        |> Enum.map(fn {symbol, position} -> {symbol, StockPositionSnapshot.new(position)} end)
    }
  end

  def combine(
        %BrokerageSnapshot{uninvested_balance: uninvested_balance1, positions: positions1},
        %BrokerageSnapshot{uninvested_balance: uninvested_balance2, positions: positions2}
      ) do
    %BrokerageSnapshot{
      uninvested_balance: Money.add(uninvested_balance1, uninvested_balance2),
      positions:
        positions1 |> Map.new() |> Map.merge(positions2 |> Map.new(), &combine_stock_positions/3)
    }
  end

  defp combine_stock_positions(_, stock_position1, stock_position2) do
    StockPositionSnapshot.combine(stock_position1, stock_position2)
  end

  def total_balance(snapshot = %BrokerageSnapshot{}, stock_ticker) do
    Money.add(
      snapshot |> invested_balance(stock_ticker),
      snapshot |> uninvested_balance()
    )
  end

  def invested_balance(%BrokerageSnapshot{positions: positions}, stock_ticker) do
    positions
    |> Enum.map(fn {symbol, position} ->
      position |> StockPositionSnapshot.value(stock_ticker |> StockTicker.price_for(symbol))
    end)
    |> Enum.reduce(Money.new(0, :USD), fn value, total -> Money.add(total, value) end)
  end

  def uninvested_balance(%BrokerageSnapshot{uninvested_balance: uninvested_balance}) do
    uninvested_balance
  end

  def positions(%BrokerageSnapshot{positions: positions}) do
    positions
  end
end
