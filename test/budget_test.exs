defmodule BudgetTest do
  use ExUnit.Case
  doctest Moneyfriend

  describe "with a variety of transactions and budgets" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Card Discover $0",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner",
            "2020/01/04 Charge Discover $10 Coffee Starbucks",
            "2020/01/04 Charge Discover $15 Restaurant McDonald's"
          ]
          |> Moneyfriend.process()
      }
    end

    test "there are budget ledgers per category", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Coffee") ==
               [
                 "-$10.00",
                 "          -         $10.00    2020/01/04    Starbucks       Discover Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$20.50",
                 "          -         $15.00    2020/01/04    McDonald's      Discover Card",
                 "-$5.50",
                 "          -          $5.50    2020/01/02    CozyCorner      Visa Card",
                 "$0.00"
               ]
    end

    test "there are budget summaries per month", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/01") ==
               [
                 "Coffee: -$10.00",
                 "Restaurant: -$20.50"
               ]
    end

    test "there are budget ledgers per category per year", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_year()
             |> Map.get("2020")
             |> Map.get("Coffee") ==
               [
                 "-$10.00",
                 "          -         $10.00    2020/01/04    Starbucks       Discover Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_year()
             |> Map.get("2020")
             |> Map.get("Restaurant") ==
               [
                 "-$20.50",
                 "          -         $15.00    2020/01/04    McDonald's      Discover Card",
                 "-$5.50",
                 "          -          $5.50    2020/01/02    CozyCorner      Visa Card",
                 "$0.00"
               ]
    end

    test "there are budget summaries per year", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_year()
             |> Map.get("2020") ==
               [
                 "Coffee: -$10.00",
                 "Restaurant: -$20.50"
               ]
    end

    test "there are budget summaries per category", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_month()
             |> Map.get("Coffee") ==
               [
                 "2020/01: -$10.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_month()
             |> Map.get("Restaurant") ==
               [
                 "2020/01: -$20.50"
               ]
    end

    test "there are budget summaries per category per year", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_year()
             |> Map.get("Coffee") ==
               [
                 "2020: -$10.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_year()
             |> Map.get("Restaurant") ==
               [
                 "2020: -$20.50"
               ]
    end
  end

  describe "with a budget that is net income" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Bank Ally $0",
            "2020/01/04 Deposit $100 Into Ally Bank Gift Parents"
          ]
          |> Moneyfriend.process()
      }
    end

    test "there are budget ledgers per category", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Gift") ==
               [
                 "$100.00",
                 "          +        $100.00    2020/01/04    Parents         Ally Bank",
                 "$0.00"
               ]
    end

    test "there are budget summaries per month", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/01") ==
               [
                 "Gift: $100.00"
               ]
    end

    test "there are budget ledgers per category per year", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_year()
             |> Map.get("2020")
             |> Map.get("Gift") ==
               [
                 "$100.00",
                 "          +        $100.00    2020/01/04    Parents         Ally Bank",
                 "$0.00"
               ]
    end

    test "there are budget summaries per year", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_year()
             |> Map.get("2020") ==
               [
                 "Gift: $100.00"
               ]
    end

    test "there are budget summaries per category", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_month()
             |> Map.get("Gift") ==
               [
                 "2020/01: $100.00"
               ]
    end

    test "there are budget summaries per category per year", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_year()
             |> Map.get("Gift") ==
               [
                 "2020: $100.00"
               ]
    end
  end

  describe "with transactions across different months and years" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Card Discover $0",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner",
            "2020/01/04 Charge Discover $10 Coffee Starbucks",
            "2020/02/04 Charge Discover $15 Restaurant McDonald's",
            "2021/04/04 Charge Visa $5 Coffee Starbucks"
          ]
          |> Moneyfriend.process()
      }
    end

    test "there are budget ledgers per category per month", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Coffee") ==
               [
                 "-$10.00",
                 "          -         $10.00    2020/01/04    Starbucks       Discover Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$5.50",
                 "          -          $5.50    2020/01/02    CozyCorner      Visa Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/02")
             |> Map.get("Restaurant") ==
               [
                 "-$15.00",
                 "          -         $15.00    2020/02/04    McDonald's      Discover Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2021/04")
             |> Map.get("Coffee") ==
               [
                 "-$5.00",
                 "          -          $5.00    2021/04/04    Starbucks       Visa Card",
                 "$0.00"
               ]
    end

    test "monthly budget summaries separate budgets per month", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/01") ==
               [
                 "Coffee: -$10.00",
                 "Restaurant: -$5.50"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/02") ==
               [
                 "Restaurant: -$15.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2021/04") ==
               [
                 "Coffee: -$5.00"
               ]
    end

    test "there are budget ledgers per category per year", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_year()
             |> Map.get("2020")
             |> Map.get("Restaurant") ==
               [
                 "-$20.50",
                 "          -         $15.00    2020/02/04    McDonald's      Discover Card",
                 "-$5.50",
                 "          -          $5.50    2020/01/02    CozyCorner      Visa Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_year()
             |> Map.get("2020")
             |> Map.get("Coffee") ==
               [
                 "-$10.00",
                 "          -         $10.00    2020/01/04    Starbucks       Discover Card",
                 "$0.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_year()
             |> Map.get("2021")
             |> Map.get("Coffee") ==
               [
                 "-$5.00",
                 "          -          $5.00    2021/04/04    Starbucks       Visa Card",
                 "$0.00"
               ]
    end

    test "yearly budget summaries separate budgets by year", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_year()
             |> Map.get("2020") ==
               [
                 "Coffee: -$10.00",
                 "Restaurant: -$20.50"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_year()
             |> Map.get("2021") ==
               [
                 "Coffee: -$5.00"
               ]
    end

    test "per-category budget summaries show different months", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_month()
             |> Map.get("Coffee") ==
               [
                 "2021/04: -$5.00",
                 "2020/01: -$10.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_month()
             |> Map.get("Restaurant") ==
               [
                 "2020/02: -$15.00",
                 "2020/01: -$5.50"
               ]
    end

    test "per-category-per-year budget summaries group by year", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_year()
             |> Map.get("Coffee") ==
               [
                 "2021: -$5.00",
                 "2020: -$10.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_year()
             |> Map.get("Restaurant") ==
               [
                 "2020: -$20.50"
               ]
    end
  end

  describe "with transactions across more than three different months" do
    setup do
      {
        :ok,
        friend:
          [
            "2020/01/01 Add Card Visa $12.10",
            "2020/01/01 Add Card Discover $0",
            "2020/01/02 Charge Visa $5.50 Restaurant CozyCorner",
            "2020/01/04 Charge Discover $10 Coffee Starbucks",
            "2020/02/04 Charge Discover $15 Restaurant McDonald's",
            "2020/03/04 Charge Discover $20 Restaurant McDonald's",
            "2020/04/04 Charge Discover $25 Restaurant McDonald's"
          ]
          |> Moneyfriend.process()
      }
    end

    test "there are budget ledgers for old months", context do
      assert context[:friend]
             |> Moneyfriend.budget_ledgers_by_month()
             |> Map.get("2020/01")
             |> Map.get("Restaurant") ==
               [
                 "-$5.50",
                 "          -          $5.50    2020/01/02    CozyCorner      Visa Card",
                 "$0.00"
               ]
    end

    test "monthly budget summaries show all months", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/01") ==
               [
                 "Coffee: -$10.00",
                 "Restaurant: -$5.50"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/02") ==
               [
                 "Restaurant: -$15.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/03") ==
               [
                 "Restaurant: -$20.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_month()
             |> Map.get("2020/04") ==
               [
                 "Restaurant: -$25.00"
               ]
    end

    test "per-category summaries show all months", context do
      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_month()
             |> Map.get("Coffee") ==
               [
                 "2020/01: -$10.00"
               ]

      assert context[:friend]
             |> Moneyfriend.budgets_by_category_per_month()
             |> Map.get("Restaurant") ==
               [
                 "2020/04: -$25.00",
                 "2020/03: -$20.00",
                 "2020/02: -$15.00",
                 "2020/01: -$5.50"
               ]
    end
  end
end
