defmodule MapUtil do
  def map_values(map, transform_fun) do
    map
    |> Enum.reduce(%{}, fn {key, value}, result ->
      result |> Map.put(key, transform_fun.(value))
    end)
  end

  def map_keys(map, transform_fun) do
    map
    |> Enum.reduce(%{}, fn {key, value}, result ->
      result |> Map.put(transform_fun.(key), value)
    end)
  end

  def expand_with_nested_keys(map) do
    map
    |> Enum.group_by(fn {{key1, _}, _} -> key1 end)
    |> MapUtil.map_values(fn arr ->
      arr |> Map.new(fn {{_, key2}, value} -> {key2, value} end)
    end)
  end
end
