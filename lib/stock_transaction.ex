defmodule StockTransaction do
  defstruct count: 0, date: "", description: ""

  def new(count, date, description) do
    %StockTransaction{count: count, date: date, description: description}
  end
end
