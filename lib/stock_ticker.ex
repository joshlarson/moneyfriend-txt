defmodule StockTicker do
  def new() do
    %{}
  end

  def price_for(ticker, symbol) do
    ticker |> Map.get(symbol)
  end

  def update_stock_price(ticker, symbol, price) do
    ticker |> Map.put(symbol, price)
  end
end
