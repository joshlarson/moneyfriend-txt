defmodule InputProcessor do
  def apply_command_to_friend(line, friend) do
    args = String.split(line)

    friend
    |> apply_date(args)
    |> apply_args(args)
  end

  defp apply_date(friend, [date | _]) do
    friend |> Friend.update_date(date)
  end

  defp apply_date(friend, []) do
    friend
  end

  defp apply_args(friend, args) do
    apply_args_to_friend(args, friend)
  end

  defp apply_args_to_friend([_, "Add", "Card", card_name, balance_str], friend) do
    {:ok, balance} = Money.parse(balance_str)

    friend |> Friend.add_card(card_name, balance)
  end

  defp apply_args_to_friend([date, "Charge", card_name, amount_str, category, vendor], friend) do
    case Money.parse(amount_str) do
      {:ok, amount_money} ->
        transaction = Transaction.of(amount_money, date)

        friend
        |> Friend.charge_card(
          card_name,
          transaction |> Transaction.for(category, vendor)
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(vendor, "#{card_name} Card") |> Transaction.reverse()
        )

      :error ->
        friend |> Friend.add_warning("Invalid Dollar Amount: '#{amount_str}'")
    end
  end

  defp apply_args_to_friend([date, "Refund", card_name, amount_str, category, vendor], friend) do
    case Money.parse(amount_str) do
      {:ok, amount_money} ->
        transaction = Transaction.of(amount_money, date)

        friend
        |> Friend.charge_card(
          card_name,
          transaction
          |> Transaction.for(category, vendor)
          |> Transaction.reverse()
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(vendor, "#{card_name} Card")
        )

      :error ->
        friend |> Friend.add_warning("Invalid Dollar Amount: '#{amount_str}'")
    end
  end

  defp apply_args_to_friend([_, "Add", "Bank", bank_name, balance_str], friend) do
    {:ok, balance} = Money.parse(balance_str)

    friend |> Friend.add_bank(bank_name, balance)
  end

  defp apply_args_to_friend(
         [date, "Deposit", amount_str, "Into", bank_name, "Bank", category, from],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.deposit(
          bank_name,
          transaction |> Transaction.for(category, from)
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(from, "#{bank_name} Bank")
        )

      :error ->
        friend |> Friend.add_warning("Invalid Dollar Amount: '#{amount_str}'")
    end
  end

  defp apply_args_to_friend([date, "Pay", card_name, amount_str, "From", bank_name], friend) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        friend |> Friend.pay_card_balance(card_name, bank_name, Transaction.of(amount, date))
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Pay",
           amount_str,
           "From",
           bank_name,
           "Bank",
           category,
           vendor
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date) |> Transaction.reverse()

        friend
        |> Friend.withdraw(
          bank_name,
          transaction
          |> Transaction.for(category, vendor)
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(vendor, "#{bank_name} Bank")
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Transfer",
           amount_str,
           "From",
           from_bank_name,
           "Bank",
           "To",
           to_bank_name,
           "Bank"
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.withdraw(
          from_bank_name,
          transaction
          |> Transaction.for("Transfer To", "#{to_bank_name} Bank")
          |> Transaction.reverse()
        )
        |> Friend.withdraw(
          to_bank_name,
          transaction
          |> Transaction.for("Transfer From", "#{from_bank_name} Bank")
        )
    end
  end

  defp apply_args_to_friend([_date, "Close", bank_name, "Bank"], friend) do
    friend
    |> Friend.close_bank(bank_name)
  end

  defp apply_args_to_friend([date, "Add", "Cash", amount_str], friend) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        friend |> Friend.add_cash(Transaction.of(amount, date) |> Transaction.for("Add", ""))
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Withdraw",
           amount_str,
           "From",
           bank_name,
           "Bank",
           "As",
           "Cash",
           category,
           fee_str
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        case Money.parse(fee_str) do
          {:ok, fee} ->
            transaction = Transaction.of(amount, date)
            fee_transaction = Transaction.of(fee, date) |> Transaction.reverse()

            friend
            |> Friend.add_cash(
              transaction
              |> Transaction.for("ATM Withdrawal", "From #{bank_name} Bank")
            )
            |> Friend.withdraw(
              bank_name,
              transaction
              |> Transaction.for("Withdrawal As", "Cash")
              |> Transaction.reverse()
            )
            |> Friend.withdraw(
              bank_name,
              fee_transaction |> Transaction.for("Fee", "ATM")
            )
            |> Friend.update_budget(
              date,
              category,
              fee_transaction |> Transaction.for("ATM Fee At", "#{bank_name} Bank")
            )
        end
    end
  end

  defp apply_args_to_friend([date, "Pay", "Cash", amount_str, category, vendor], friend) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.spend_cash(transaction |> Transaction.for(category, vendor))
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(vendor, "Cash") |> Transaction.reverse()
        )
    end
  end

  defp apply_args_to_friend([date, "Receive", "Cash", amount_str, category, vendor], friend) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.spend_cash(
          transaction
          |> Transaction.for(category, vendor)
          |> Transaction.reverse()
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(vendor, "Cash")
        )
    end
  end

  defp apply_args_to_friend(
         [
           _,
           "Add",
           "P2P",
           p2p_name,
           starting_balance_str,
           "Funded",
           "By",
           bank_name
         ],
         friend
       ) do
    case Money.parse(starting_balance_str) do
      {:ok, starting_balance} ->
        friend |> Friend.add_p2p(p2p_name, starting_balance, bank_name)
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Receive",
           amount_str,
           "On",
           p2p_name,
           "From",
           name,
           category,
           vendor
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.receive_p2p(
          p2p_name,
          transaction |> Transaction.for(category, "#{vendor} / #{name}")
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(vendor, "#{p2p_name} / #{name}")
        )
    end
  end

  defp apply_args_to_friend(
         [date, "Pay", name, amount_str, "On", p2p_name, category, vendor],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.pay_p2p(
          p2p_name,
          transaction |> Transaction.for(category, "#{vendor} / #{name}")
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for(vendor, "#{p2p_name} / #{name}") |> Transaction.reverse()
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Transfer",
           amount_str,
           "From",
           "P2P",
           p2p_name,
           "To",
           bank_name,
           "Bank"
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.pay_p2p(
          p2p_name,
          transaction |> Transaction.for("Transfer To", "#{bank_name} Bank")
        )
        |> Friend.deposit(
          bank_name,
          transaction |> Transaction.for("Transfer From", p2p_name)
        )
    end
  end

  defp apply_args_to_friend(
         [_date, "Change", "Funding", "For", p2p_name, "To", bank_name],
         friend
       ) do
    friend |> Friend.update_p2p_funding(p2p_name, bank_name)
  end

  defp apply_args_to_friend([_, "Add", "Splitwise", splitwise_name, starting_balance_str], friend) do
    case Money.parse(starting_balance_str) do
      {:ok, starting_balance} ->
        friend |> Friend.add_splitwise(splitwise_name, starting_balance)
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Splitwise",
           "Pay",
           amount_str,
           group_name,
           share_str,
           category,
           vendor
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        share =
          case Float.parse(share_str) do
            {percent, "%"} ->
              amount |> Money.multiply(percent) |> Money.divide(100) |> List.first()

            _ ->
              case Money.parse(share_str) do
                {:ok, amount} -> amount
              end
          end

        transaction = Transaction.of(Money.subtract(amount, share), date)

        friend
        |> Friend.pay_splitwise(
          group_name,
          transaction
          |> Transaction.for(
            vendor,
            "Pay #{Money.to_string(amount)} (My share was #{Money.to_string(share)})"
          )
        )
        |> Friend.update_budget(
          date,
          category,
          transaction
          |> Transaction.for(
            vendor,
            "Splitwise / #{group_name} / Paid #{Money.to_string(amount)}, my share was #{Money.to_string(share)}"
          )
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Splitwise",
           "Owe",
           amount_str,
           group_name,
           share_str,
           category,
           vendor
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        case Float.parse(share_str) do
          {percent, "%"} ->
            share = amount |> Money.multiply(percent) |> Money.divide(100) |> List.first()

            transaction = Transaction.of(share, date)

            friend
            |> Friend.owe_splitwise(
              group_name,
              transaction
              |> Transaction.for(
                vendor,
                "Owe #{Money.to_string(share)} (Total was #{Money.to_string(amount)})"
              )
            )
            |> Friend.update_budget(
              date,
              category,
              transaction
              |> Transaction.for(
                vendor,
                "Splitwise / #{group_name} / #{percent}% of #{Money.to_string(amount)}"
              )
              |> Transaction.reverse()
            )
        end
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Splitwise",
           "Owe",
           group_name,
           share_str,
           category,
           vendor
         ],
         friend
       ) do
    case Money.parse(share_str) do
      {:ok, share} ->
        transaction = Transaction.of(share, date)

        friend
        |> Friend.owe_splitwise(
          group_name,
          transaction |> Transaction.for(vendor, "Owe #{Money.to_string(share)}")
        )
        |> Friend.update_budget(
          date,
          category,
          transaction
          |> Transaction.for(vendor, "Splitwise / #{group_name}")
          |> Transaction.reverse()
        )
    end
  end

  defp apply_args_to_friend([_date, "Close", "Splitwise", group_name], friend) do
    friend
    |> Friend.close_splitwise(group_name)
  end

  defp apply_args_to_friend([_, "Add", "Brokerage", brokerage_name, starting_balance_str], friend) do
    case Money.parse(starting_balance_str) do
      {:ok, starting_balance} ->
        friend |> Friend.add_brokerage(brokerage_name, starting_balance)
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Transfer",
           amount_str,
           "From",
           bank_name,
           "Bank",
           "To",
           brokerage_name,
           "Brokerage"
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        friend
        |> Friend.withdraw(
          bank_name,
          Transaction.of(amount, date)
          |> Transaction.for("Transfer To", brokerage_name)
          |> Transaction.reverse()
        )
        |> Friend.add_brokerage_value(
          brokerage_name,
          Transaction.of(amount, date) |> Transaction.for("Transfer From", "#{bank_name} Bank")
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Transfer",
           amount_str,
           "From",
           brokerage_name,
           "Brokerage",
           "To",
           bank_name,
           "Bank"
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        friend
        |> Friend.add_brokerage_value(
          brokerage_name,
          Transaction.of(amount, date)
          |> Transaction.for("Transfer To", "#{bank_name} Bank")
          |> Transaction.reverse()
        )
        |> Friend.deposit(
          bank_name,
          Transaction.of(amount, date) |> Transaction.for("Transfer From", brokerage_name)
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Transfer",
           amount_str,
           "From",
           from_brokerage_name,
           "Brokerage",
           "To",
           to_brokerage_name,
           "Brokerage"
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.add_brokerage_value(
          from_brokerage_name,
          transaction
          |> Transaction.for("Transfer To", "#{to_brokerage_name} Brokerage")
          |> Transaction.reverse()
        )
        |> Friend.add_brokerage_value(
          to_brokerage_name,
          transaction |> Transaction.for("Transfer From", "#{from_brokerage_name} Brokerage")
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Deposit",
           amount_str,
           "Into",
           brokerage_name,
           "Brokerage",
           category
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction = Transaction.of(amount, date)

        friend
        |> Friend.add_brokerage_value(
          brokerage_name,
          transaction |> Transaction.for("Deposit", category)
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for("Deposit Into", brokerage_name)
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Deduct",
           amount_str,
           "From",
           brokerage_name,
           "Brokerage",
           category
         ],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount} ->
        transaction =
          Transaction.of(amount, date)
          |> Transaction.reverse()

        friend
        |> Friend.add_brokerage_value(
          brokerage_name,
          transaction |> Transaction.for("Deduction", category)
        )
        |> Friend.update_budget(
          date,
          category,
          transaction |> Transaction.for("Deduction From", brokerage_name)
        )
    end
  end

  defp apply_args_to_friend(
         [
           date,
           "Buy",
           count_str,
           "Shares",
           symbol,
           "On",
           brokerage_name,
           "At",
           price_str
         ],
         friend
       ) do
    {count, ""} = Float.parse(count_str)
    {:ok, price} = Money.parse(price_str)

    friend
    |> Friend.buy_stocks(brokerage_name, symbol, StockTransaction.new(count, date, "Buy"), price)
  end

  defp apply_args_to_friend(
         [
           date,
           "Sell",
           count_str,
           "Shares",
           symbol,
           "On",
           brokerage_name,
           "At",
           price_str
         ],
         friend
       ) do
    {count, ""} = Float.parse(count_str)
    {:ok, price} = Money.parse(price_str)

    friend
    |> Friend.buy_stocks(
      brokerage_name,
      symbol,
      StockTransaction.new(-count, date, "Sell"),
      price
    )
  end

  defp apply_args_to_friend(
         [
           date,
           "Grant",
           count_str,
           "Shares",
           symbol,
           "On",
           brokerage_name
         ],
         friend
       ) do
    {count, ""} = Float.parse(count_str)

    friend
    |> Friend.grant_stocks(brokerage_name, symbol, StockTransaction.new(count, date, "Grant"))
  end

  defp apply_args_to_friend([_, "Stock", "Price", "Of", symbol, "Is", price_str], friend) do
    {:ok, price} = Money.parse(price_str)
    friend |> Friend.update_stock_price(symbol, price)
  end

  defp apply_args_to_friend(
         [date, "Contribute", amount_str, "From", category_from, "To", category_to],
         friend
       ) do
    case Money.parse(amount_str) do
      {:ok, amount_money} ->
        transaction = Transaction.of(amount_money, date)

        friend
        |> Friend.update_budget(
          date,
          category_to,
          transaction |> Transaction.for("Contrib. From", category_from) |> Transaction.reverse()
        )
        |> Friend.update_budget(
          date,
          category_from,
          transaction |> Transaction.for("Contrib. To", category_to)
        )
    end
  end

  defp apply_args_to_friend([], friend) do
    friend
  end

  defp apply_args_to_friend(["#" | _], friend) do
    friend
  end

  defp apply_args_to_friend(args, friend) do
    command = args |> Enum.join(" ")
    friend |> Friend.add_warning("Unrecognized Command: '#{command}'")
  end
end
