defmodule Ledger do
  def new() do
    [Money.new(0, :USD)]
  end

  def new(starting_balance) do
    [starting_balance]
  end

  def balance([balance | _]) do
    balance
  end

  def apply_transaction(ledger = [balance | _], transaction = %Transaction{amount: amount}) do
    [Money.add(balance, amount) | [transaction | ledger]]
  end
end
