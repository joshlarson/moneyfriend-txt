defmodule SplitwisePrinter do
  def summary({name, splitwise}) do
    "#{name}: #{balance_description(Splitwise.balance(splitwise))}"
  end

  defp balance_description(%Money{amount: amount}) when amount == 0 do
    "Settled up"
  end

  defp balance_description(balance = %Money{amount: amount}) when amount > 0 do
    "You are owed #{Money.to_string(balance)}"
  end

  defp balance_description(balance = %Money{amount: amount}) when amount < 0 do
    "You owe #{balance |> Money.neg() |> Money.to_string()}"
  end

  def ledger({name, %Splitwise{ledger: ledger}}) do
    {name, LedgerPrinter.print_ledger(ledger)}
  end
end
