defmodule CashPrinter do
  def summary(cash) do
    case cash |> Cash.balance() do
      %Money{amount: 0} -> []
      balance -> [balance |> Money.to_string()]
    end
  end

  def ledger(%Cash{ledger: ledger}) do
    LedgerPrinter.print_ledger(ledger)
  end
end
