defmodule StockPosition do
  defstruct ledger: [0.0]

  def count(%StockPosition{ledger: [count | _]}) do
    count
  end

  def add_shares(
        stock_position = %StockPosition{ledger: ledger = [old_count | _]},
        stock_transaction = %StockTransaction{count: count}
      ) do
    new_count = count + old_count
    new_ledger = [new_count | [stock_transaction | ledger]]
    %StockPosition{stock_position | ledger: new_ledger}
  end
end
